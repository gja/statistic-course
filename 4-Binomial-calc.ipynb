{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binomial calculator  \n",
    "*Version 2023*  \n",
    "\n",
    "The binomial distribution is given by\n",
    "\n",
    "$$f_n(r) = \\frac{n!}{r!(n-r!)} p^r (1-p)^{n-r}$$\n",
    "\n",
    "This is the probability of $r$ successes out of $n$ trials, if the probability of success at each trial is $p$. Below we provide code cells to calculate (i) a single specific probabiity for a given $p,n,r$, (ii) the cumulative probability at $r$ and above for a given $n$, and (iii) the complete set of $f(r)$ values for a given $n$. Finally, we plot the $f(r)$ values.  \n",
    "\n",
    "As usual, do Cell/Run All first, then you can edit parameters for specific cells. First, standard set-up."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import *  # basic maths routines\n",
    "import numpy as np \n",
    "from scipy import stats\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (1) Single $f_n(r)$ values\n",
    "\n",
    "In the code cell below, edit in the values of $p,n,r$ you want and then run the cell. Note that we have rounded down to two decimal places, but you can change that if you wish by uncommenting the extra line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "p= 0.3 n= 6 r= 2   ==> f= 0.32\n"
     ]
    }
   ],
   "source": [
    "# Binomial calculator\n",
    "p=0.3\n",
    "n=6\n",
    "r=2\n",
    "f=stats.binom.pmf(r,n,p) # \"pmf\" stands for \"probability mass function\"\n",
    "print('p=',p,'n=',n,'r=',r,'  ==> f=',round(f,2))\n",
    "# print('p=',p,'n=',n,'r=',r,'  ==> f=',f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Its not hard to code up the binomial function yourself. Below is how you would do it. *You could try writing some extra code and comparing the results to see if you get the same answer...*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Home grown Binomial calculator : P(r,n,p)\n",
    "# prob of r successes out of n trials if expected fraction is p\n",
    "def Bin(r,n,p) :\n",
    " P = (factorial(n)/(factorial(r)*factorial(n-r))) * p**r * (1-p)**(n-r)\n",
    " return P"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (2) Cumulative $f_n(>r)$ values\n",
    "\n",
    "In the code cell below, edit in the values of $p,n,r$ you want and then run the cell. Note that we are resetting all of $p,n,r$ to make sure we don't accidentally re-use values from the first calculator. Note that we have used a precision of three decimal places, but you can change this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "r= 3   n= 8   p= 0.5\n",
      "summed prob at r and below= 0.363\n",
      "summed prob above r= 0.637\n",
      "(checksum= 1.0 )\n"
     ]
    }
   ],
   "source": [
    "# Cumulative binomial calculator\n",
    "p=0.5\n",
    "n=8\n",
    "r=3\n",
    "fle=round(stats.binom.cdf(r,n,p),3) # summed prob at r and below\n",
    "fge=round(stats.binom.sf(r,n,p),3) # summed prob above r\n",
    "checksum=fge+fle\n",
    "print('r=',r,'  n=',n,'  p=',p)\n",
    "print('summed prob at r and below=',fle) # \"cdf\"= cumulative distbn fn.\n",
    "print('summed prob above r=',fge)  # \"sf\" = survival function\n",
    "print('(checksum=',checksum,')')\n",
    "# note the \"survival function\" should be 1-cdf, but the Scipy docs\n",
    "# say that sf is sometimes more accurate\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (3) Complete set of  $f_n(r)$ values\n",
    "\n",
    "Here we set $p,n$ and then calculate $f(r)$ for all the possible $r$ values. Careful - if you set a large value of $n$ you will get a long output cell!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "p= 0.3 n= 8\n",
      "r= 0 f(r)= 0.05765\n",
      "r= 1 f(r)= 0.19765\n",
      "r= 2 f(r)= 0.29648\n",
      "r= 3 f(r)= 0.25412\n",
      "r= 4 f(r)= 0.13614\n",
      "r= 5 f(r)= 0.04668\n",
      "r= 6 f(r)= 0.01\n",
      "r= 7 f(r)= 0.00122\n",
      "r= 8 f(r)= 7e-05\n"
     ]
    }
   ],
   "source": [
    "# set parameters\n",
    "n=8 # total number of trials\n",
    "p=0.3 # probability of success per trial\n",
    "\n",
    "xr=np.arange(0,n+1) # set up array of r values\n",
    "fr=np.zeros(len(xr)) # initialise array to hold f(r) values\n",
    "\n",
    "# make fr\n",
    "for i in range(0,len(xr)):\n",
    "    fr[i]=round(stats.binom.pmf(xr[i],n,p),5)\n",
    "    \n",
    "print('p=',p,'n=',n,)\n",
    "for i in range(0,len(xr)):\n",
    "    print('r=',xr[i],'f(r)=',fr[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (4) Plot the distribution\n",
    "\n",
    "We will mark the mean and variance as well as plotting the distribution.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAArwAAAIhCAYAAACsQmneAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjcuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8pXeV/AAAACXBIWXMAAA9hAAAPYQGoP6dpAABYDUlEQVR4nO3deVxV1f7/8fcRZHAAldkLIs44IuAA5nRVnNOyKzaYpJam5VRWTjl0b6Rpzpo2ON0cKrXMLENzvFoZit3SykzFqxBqBc4o7N8f/jzfjoACgue4fT0fj/N4cNZZe+3P3hzw7WKdvS2GYRgCAAAATKqEvQsAAAAAihOBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBF4BDWbx4sSwWi83Dx8dHrVq10vr163P0t1gsmjBhwp0vtACuH9PRo0eLbdsbz5ubm5v8/f3VunVrxcfHKy0tLcc2EyZMkMViKVA9Fy5c0IQJE7R169YCbZfbvipXrqwuXboUaJxbWb58uWbMmJHra3fDewVA8XC2dwEAkJtFixapVq1aMgxDqampmjNnjrp27ap169apa9eu1n67d+9WYGCgHSu9tc6dO2v37t0KCAgo9n1dP29XrlxRWlqadu7cqcmTJ2vq1KlatWqV2rZta+3bv39/dejQoUDjX7hwQRMnTpQktWrVKt/bFWZfhbF8+XJ9//33GjZsWI7X7ob3CoDiQeAF4JDq1q2ryMhI6/MOHTqofPnyWrFihU3gbdq0qT3KKxAfHx/5+PjckX3deN569Oih4cOH67777tODDz6oQ4cOyc/PT5IUGBhY7AHwwoULKlWq1B3Z163cDe8VAMWDJQ0A7gpubm5ycXFRyZIlbdpv/DP19T/tb9myRU8//bS8vb3l5eWlBx98UCdPnrTZNjs7W1OmTFGtWrXk6uoqX19fPf744/rf//5n069Vq1aqW7eudu/erejoaLm7u6ty5cpatGiRJOnTTz9VeHi4SpUqpXr16unzzz+32T63ZQkJCQnq1q2bAgMD5ebmpmrVqmnAgAE6ffp0EZwtW5UqVdK0adN09uxZLViwwNqe2zKDL7/8Uq1atZKXl5fc3d1VqVIl9ejRQxcuXNDRo0etwX3ixInW5RNxcXE24+3du1cPPfSQypcvr6pVq+a5r+vWrl2r+vXry83NTVWqVNGsWbNsXs9rWcfWrVtlsVisyytatWqlTz/9VMeOHbNZ3nFdbksavv/+e3Xr1k3ly5eXm5ubwsLCtGTJklz3s2LFCo0ZM0YVK1aUh4eH2rZtq59++invEw/AYTDDC8AhZWVl6erVqzIMQ7/99ptef/11nT9/Xo888ki+tu/fv786d+6s5cuX6/jx4xo5cqQee+wxffnll9Y+Tz/9tBYuXKhnnnlGXbp00dGjRzVu3Dht3bpVe/fulbe3t7VvamqqnnjiCb3wwgsKDAzU7Nmz1bdvXx0/flwffvihRo8eLU9PT02aNEndu3fXr7/+qooVK+ZZ3+HDhxUVFaX+/fvL09NTR48e1RtvvKH77rtP//3vf3ME+9vVqVMnOTk5afv27Xn2OXr0qDp37qzmzZvr3XffVbly5XTixAl9/vnnyszMVEBAgD7//HN16NBB/fr1U//+/SUpx+z1gw8+qF69emngwIE6f/78TetKSkrSsGHDNGHCBPn7++u9997T0KFDlZmZqeeff75Axzhv3jw99dRTOnz4sNauXXvL/j/99JOio6Pl6+urWbNmycvLS//+978VFxen3377TS+88IJN/9GjR6tZs2Z6++23lZGRoRdffFFdu3bVwYMH5eTkVKBaAdxZBF4ADunGPz+7urpqzpw5at++fb6279Chg81M4e+//64XXnhBqamp8vf3148//qiFCxdq0KBBmj17trVfw4YN1aRJE02fPl3/+te/rO1nzpzRxo0bFRERIUmKjIyUr6+vXnvtNf3yyy/WcFuxYkWFhYVp9erVevbZZ/Osb+DAgdavDcNQdHS0WrVqpeDgYH322We6//7783Wc+VW6dGl5e3vnmOX+q8TERF26dEmvv/66GjRoYG3/638yrh9/YGBgnksE+vTpY13neysnT57Uvn37rPvr2LGj0tLS9Morr2jQoEEqVapUvsaRpNq1a6tcuXJydXXN1/KFCRMmKDMzU1u2bFFQUJCka/8x+PPPPzVx4kQNGDBAnp6eNuP/+9//tj53cnJSz549tWfPHpZLAA6OJQ0AHNLSpUu1Z88e7dmzR5999pn69OmjwYMHa86cOfna/sbAWL9+fUnSsWPHJElbtmyRJOuf469r3LixQkNDtXnzZpv2gIAAa9iTpAoVKsjX11dhYWE2M7mhoaE2+8lLWlqaBg4cqKCgIDk7O6tkyZIKDg6WJB08eDBfx1hQhmHc9PWwsDC5uLjoqaee0pIlS/Trr78Waj89evTId986derYhGvpWsDOyMjQ3r17C7X//Pryyy/Vpk0ba9i9Li4uThcuXNDu3btt2m/1ngLguJjhBeCQQkNDc3xo7dixY3rhhRf02GOPqVy5cjfd3svLy+a5q6urJOnixYuSrs3YSsr1ygkVK1bMEWIqVKiQo5+Li0uOdhcXF0nSpUuX8qwtOztbMTExOnnypMaNG6d69eqpdOnSys7OVtOmTa01FqXz58/rzJkzqlevXp59qlatqk2bNmnKlCkaPHiwzp8/rypVqmjIkCEaOnRovvdVkKtR+Pv759l2/XtUXM6cOZPn9z+3/d/qPQXAcTHDC+CuUb9+fV28eFE///zzbY91PbykpKTkeO3kyZM263eL2vfff6/9+/fr9ddf17PPPqtWrVqpUaNGOQJVUfr000+VlZV1y0uJNW/eXJ988onS09P11VdfKSoqSsOGDdPKlSvzva+CXNs3NTU1z7br58PNzU2SdPnyZZt+t/sBPy8vrzy//5KK9T0A4M4i8AK4ayQlJUnK+SGpwvj73/8uSTZrMiVpz549OnjwoNq0aXPb+8jL9UB4fYbwur9eQaEoJScn6/nnn5enp6cGDBiQr22cnJzUpEkTzZ07V5KsywuKelbzhx9+0P79+23ali9frrJlyyo8PFzStRtUSNJ3331n02/dunU5xnN1dc13bW3atNGXX36ZY13z0qVLVapUKdblAibCkgYADun777/X1atXJV370/KaNWuUkJCgBx54QCEhIbc9fs2aNfXUU09p9uzZKlGihDp27Gi9SkNQUJCGDx9+2/vIS61atVS1alW99NJLMgxDFSpU0CeffKKEhITbHvv6ebt69arS0tK0Y8cOLVq0SE5OTlq7du1N/7Pw5ptv6ssvv1Tnzp1VqVIlXbp0Se+++64kWW9YUbZsWQUHB+vjjz9WmzZtVKFCBXl7e1tDaUFVrFhR999/vyZMmKCAgAD9+9//VkJCgiZPnmz9wFqjRo1Us2ZNPf/887p69arKly+vtWvXaufOnTnGq1evntasWaP58+crIiJCJUqUsFka81fjx4/X+vXr1bp1a7388suqUKGC3nvvPX366aeaMmWKzQfWANzdCLwAHNITTzxh/drT01MhISF64403NGjQoCLbx/z581W1alW98847mjt3rjw9PdWhQwfFx8cX6/KCkiVL6pNPPtHQoUM1YMAAOTs7q23bttq0aZMqVap0W2NfP28uLi4qV66cQkND9eKLL6p///63nBkPCwvTF198ofHjxys1NVVlypRR3bp1tW7dOsXExFj7vfPOOxo5cqTuv/9+Xb58WX369NHixYsLVW9YWJieeOIJjR8/XocOHVLFihX1xhtv2PyHw8nJSZ988omeeeYZDRw4UK6ururVq5fmzJmjzp0724w3dOhQ/fDDDxo9erTS09NlGEaeH9arWbOmdu3apdGjR2vw4MG6ePGiQkNDtWjRohwfZgRwd7MYt/rYLgAAAHAXYw0vAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFPjOry5yM7O1smTJ1W2bNkC3SITAAAAd4ZhGDp79qwqVqyoEiVuPodL4M3FyZMnFRQUZO8yAAAAcAvHjx9XYGDgTfsQeHNRtmxZSddOoIeHh52rARzT6dNS1aq2bYcPS97e9qkHAHBvycjIUFBQkDW33QyBNxfXlzF4eHgQeIE8XL6cs61sWYkfGQDAnZSf5ad8aA0AAACmRuAFAACAqRF4AQAAYGqs4QUAwIEZhqGrV68qKyvL3qUAd1zJkiXl5OR02+MQeAEAcFCZmZlKSUnRhQsX7F0KYBcWi0WBgYEqU6bMbY1D4AUAwAFlZ2fryJEjcnJyUsWKFeXi4sLNkHBPMQxDp06d0v/+9z9Vr179tmZ6CbwAADigzMxMZWdnKygoSKVKlbJ3OYBd+Pj46OjRo7py5cptBV4+tAYAgAO71S1TATMrqr9q8FMEAAAAUyPwAgAAwNTsHnjnzZunkJAQubm5KSIiQjt27Miz786dO9WsWTN5eXnJ3d1dtWrV0vTp03P0W716tWrXri1XV1fVrl1ba9euLc5DAAAAgAOza+BdtWqVhg0bpjFjxmjfvn1q3ry5OnbsqOTk5Fz7ly5dWs8884y2b9+ugwcPauzYsRo7dqwWLlxo7bN7927Fxsaqd+/e2r9/v3r37q2ePXvq66+/vlOHBQDAPS0uLk4Wi0UDBw7M8dqgQYNksVgUFxd35wvDPctiGIZhr503adJE4eHhmj9/vrUtNDRU3bt3V3x8fL7GePDBB1W6dGktW7ZMkhQbG6uMjAx99tln1j4dOnRQ+fLltWLFilzHuHz5si5fvmx9npGRoaCgIKWnp8vDw6MwhwaY3qlTkq+vbVtamuTjY596ALO5dOmSjhw5Yv0raHa2dOaM/erx8pLy+/m5uLg4ffnll8rIyFBKSorc3d0lXTumgIAAeXh4qHXr1lq8eHHxFQxTuPHn4K8yMjLk6emZr7xmtxnezMxMJSYmKiYmxqY9JiZGu3btytcY+/bt065du9SyZUtr2+7du3OM2b59+5uOGR8fL09PT+sjKCioAEcCAEDxO3Pm2n8y7fUoaNgODw9XpUqVtGbNGmvbmjVrFBQUpIYNG1rbDMPQlClTVKVKFbm7u6tBgwb68MMPra9nZWWpX79+CgkJkbu7u2rWrKmZM2fa7CsuLk7du3fX1KlTFRAQIC8vLw0ePFhXrlwp3MmG6dgt8J4+fVpZWVny8/Ozaffz81NqaupNtw0MDJSrq6siIyM1ePBg9e/f3/paampqgcccNWqU0tPTrY/jx48X4ogAAMBfPfHEE1q0aJH1+bvvvqu+ffva9Bk7dqwWLVqk+fPn64cfftDw4cP12GOPadu2bZKu3YAjMDBQ77//vg4cOKCXX35Zo0eP1vvvv28zzpYtW3T48GFt2bJFS5Ys0eLFi5lBhpXdbzxx4/XVDMO45TXXduzYoXPnzumrr77SSy+9pGrVqunhhx8u9Jiurq5ydXUtRPUAACAvvXv31qhRo3T06FFZLBb95z//0cqVK7V161ZJ0vnz5/XGG2/oyy+/VFRUlCSpSpUq2rlzpxYsWKCWLVuqZMmSmjhxonXMkJAQ7dq1S++//7569uxpbS9fvrzmzJkjJycn1apVS507d9bmzZv15JNP3tFjhmOyW+D19vaWk5NTjpnXtLS0HDO0NwoJCZEk1atXT7/99psmTJhgDbz+/v6FGhMAABQtb29vde7cWUuWLJFhGOrcubO8vb2trx84cECXLl1Su3btbLbLzMy0Wfbw5ptv6u2339axY8d08eJFZWZmKiwszGabOnXq2NyJKyAgQP/973+L58Bw17Fb4HVxcVFERIQSEhL0wAMPWNsTEhLUrVu3fI9jGIbNB86ioqKUkJCg4cOHW9u++OILRUdHF03hAADYgZfXtQ+G2nP/hdG3b18988wzkqS5c+favJadnS1J+vTTT/W3v/3N5rXrf3l9//33NXz4cE2bNk1RUVEqW7asXn/99RxXXypZsqTNc4vFYh0fsOuShhEjRqh3796KjIxUVFSUFi5cqOTkZOtlTEaNGqUTJ05o6dKlkq79oFSqVEm1atWSdO26vFOnTtWzzz5rHXPo0KFq0aKFJk+erG7duunjjz/Wpk2btHPnzjt/gAAAFJESJe7Oq6B06NBBmZmZkq59iPyvrl8zPzk52eYD6H+1Y8cORUdHa9CgQda2w4cPF1/BMCW7Bt7Y2FidOXNGkyZNUkpKiurWrasNGzYoODhYkpSSkmJzTd7s7GyNGjVKR44ckbOzs6pWrarXXntNAwYMsPaJjo7WypUrNXbsWI0bN05Vq1bVqlWr1KRJkzt+fMBtKaL7h98W+121EIBJODk56eDBg9av/6ps2bJ6/vnnNXz4cGVnZ+u+++5TRkaGdu3apTJlyqhPnz6qVq2ali5dqo0bNyokJETLli3Tnj17rMsbgfyw+4fWBg0aZPO/tr+68dOVzz77rM1sbl4eeughPfTQQ0VRHgAAuE03u0bqK6+8Il9fX8XHx+vXX39VuXLlFB4ertGjR0uSBg4cqKSkJMXGxspisejhhx/WoEGDbK63D9yKXW884agKciFjoNg4+AwvN54AitfNLrgP3Cvu+htPAAAAAHcCgRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAJja0aNHZbFYlJSUdFeNXRiLFy9WuXLlHGYcR0HgBQAARSotLU0DBgxQpUqV5OrqKn9/f7Vv3167d++29rFYLProo4/sV+Qd1KpVK1ksFlksFrm6uupvf/ubunbtqjVr1hT5vmJjY/Xzzz8XaJvKlStrxowZtz2OI3O2dwEAAKBgXtn2iv654593ZF9jm4/VuJbjCrRNjx49dOXKFS1ZskRVqlTRb7/9ps2bN+v3338vpiqLX2ZmplxcXAq9/ZNPPqlJkybpypUrOnHihNauXatevXopLi5OCxcuLLI63d3d5e7u7jDjOApmeAEAuMtkGVnKzMq8I48sI6tAtf3555/auXOnJk+erNatWys4OFiNGzfWqFGj1LlzZ0nXZhQl6YEHHpDFYrE+P3z4sLp16yY/Pz+VKVNGjRo10qZNm2zGr1y5sl599VX17dtXZcuWVaVKlXIExm+++UYNGzaUm5ubIiMjtW/fPtvzl5Wlfv36KSQkRO7u7qpZs6Zmzpxp0ycuLk7du3dXfHy8KlasqBo1auRr7LyUKlVK/v7+CgoKUtOmTTV58mQtWLBAb731ls0xnjhxQrGxsSpfvry8vLzUrVs3HT16VJK0ceNGubm56c8//7QZe8iQIWrZsqWknEsRbnVOW7VqpWPHjmn48OHWWejcxpGk+fPnq2rVqnJxcVHNmjW1bNkym9ctFovefvttPfDAAypVqpSqV6+udevW5ev8FDcCLwAAKDJlypRRmTJl9NFHH+ny5cu59tmzZ48kadGiRUpJSbE+P3funDp16qRNmzZp3759at++vbp27ark5GSb7adNm2YNm4MGDdLTTz+tH3/8UZJ0/vx5denSRTVr1lRiYqImTJig559/3mb77OxsBQYG6v3339eBAwf08ssva/To0Xr//fdt+m3evFkHDx5UQkKC1q9fn6+xC6JPnz4qX768dWnDhQsX1Lp1a5UpU0bbt2/Xzp07VaZMGXXo0EGZmZlq27atypUrp9WrV1vHyMrK0vvvv69HH300133c6pyuWbNGgYGBmjRpklJSUpSSkpLrOGvXrtXQoUP13HPP6fvvv9eAAQP0xBNPaMuWLTb9Jk6cqJ49e+q7775Tp06d9OijjzrEzD6BFwAAFBlnZ2ctXrxYS5YsUbly5dSsWTONHj1a3333nbWPj4+PJKlcuXLy9/e3Pm/QoIEGDBigevXqqXr16vrnP/+pKlWq5Jgl7NSpkwYNGqRq1arpxRdflLe3t7Zu3SpJeu+995SVlaV3331XderUUZcuXTRy5Eib7UuWLKmJEyeqUaNGCgkJ0aOPPqq4uLgcgbd06dJ6++23VadOHdWtWzdfYxdEiRIlVKNGDesM7sqVK1WiRAm9/fbbqlevnkJDQ7Vo0SIlJydr69atcnJyUmxsrJYvX24dY/Pmzfrjjz/0j3/8I9d93OqcVqhQQU5OTipbtqz8/f3l7++f6zhTp05VXFycBg0apBo1amjEiBF68MEHNXXqVJt+cXFxevjhh1WtWjW9+uqrOn/+vL755ptCn6OiQuAFAABFqkePHjp58qTWrVun9u3ba+vWrQoPD9fixYtvut358+f1wgsvqHbt2ipXrpzKlCmjH3/8MccMb/369a1fWywW+fv7Ky0tTZJ08OBBNWjQQKVKlbL2iYqKyrGvN998U5GRkfLx8VGZMmX01ltv5dhPvXr1bNbt5nfsgjAMw7qMIDExUb/88ovKli1rnSmvUKGCLl26pMOHD0uSHn30UW3dulUnT56UdC3gd+rUSeXLl891/Pye01s5ePCgmjVrZtPWrFkzHTx40Kbtr9+b0qVLq2zZstbvjT3xoTUAAFDk3Nzc1K5dO7Vr104vv/yy+vfvr/HjxysuLi7PbUaOHKmNGzdq6tSpqlatmtzd3fXQQw8pMzPTpl/JkiVtnlssFmVnZ0u6FiBv5f3339fw4cM1bdo0RUVFqWzZsnr99df19ddf2/QrXbq0zfP8jF0QWVlZOnTokBo1aiTp2lKLiIgIvffeezn6Xp8Fb9y4sapWraqVK1fq6aef1tq1a7Vo0aI895Hfc5of14P5dX8N69fd7HtjTwReAADuMk4WJ7k4Ff6KAQXdV1GoXbu2zWXISpYsqaws2w/E7dixQ3FxcXrggQckXVt/ev3P/QXZz7Jly3Tx4kXrVQa++uqrHPuJjo7WoEGDrG3XZ1Bvd+yCWLJkif744w/16NFDkhQeHq5Vq1bJ19dXHh4eeW73yCOP6L333lNgYKBKlChh/TBgbvJzTl1cXHJ8L24UGhqqnTt36vHHH7e27dq1S6Ghobc6TIdA4AUA4C4zruW4Al8q7E45c+aM/vGPf6hv376qX7++ypYtq2+//VZTpkxRt27drP0qV66szZs3q1mzZnJ1dVX58uVVrVo1rVmzRl27dpXFYtG4ceMKPDv4yCOPaMyYMerXr5/Gjh2ro0eP5lhnWq1aNS1dulQbN25USEiIli1bpj179igkJOS2x87LhQsXlJqaqqtXr+rEiRNas2aNpk+frqefflqtW7eWdG25wuuvv65u3bpp0qRJCgwMVHJystasWaORI0cqMDDQ2m/ixIn617/+pYceekhubm557jc/57Ry5cravn27evXqJVdXV3l7e+cYZ+TIkerZs6fCw8PVpk0bffLJJ1qzZk2Oq2g4KtbwAgCAIlOmTBk1adJE06dPV4sWLVS3bl2NGzdOTz75pObMmWPtN23aNCUkJCgoKEgNGzaUJE2fPl3ly5dXdHS0unbtqvbt2ys8PLzA+//kk0904MABNWzYUGPGjNHkyZNt+gwcOFAPPvigYmNj1aRJE505c8Zmtvd2xs7LW2+9pYCAAFWtWlUPPPCADhw4oFWrVmnevHnWPqVKldL27dtVqVIlPfjggwoNDVXfvn118eJFmxnf6tWrq1GjRvruu+/yvDrDdfk5p5MmTdLRo0dVtWpV69KJG3Xv3l0zZ87U66+/rjp16mjBggVatGiRWrVqla/jtzeLUdQLUkwgIyNDnp6eSk9Pv+mfFIBidcO6KLu4ya+HU6ckX1/btrQ0KY/flQAK6NKlSzpy5IhCQkJuOoMHmNnNfg4KkteY4QUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBALjbWCx37lFMjh49KovFoqSkpGLbR35NmDBBYWFhBdrGYrHoo48+KtA2rVq10rBhw6zPK1eurBkzZhRojPyIi4tT9+7d89xvce7LURF4AQBAkYqLi5PFYrE+vLy81KFDB3333XfWPkFBQUpJSVHdunXtWOk1zz//vDZv3nzH97tnzx499dRT+epbkHA8c+ZMLV68uPCF5SKv/6AUx76KA4EXAAAUuQ4dOiglJUUpKSnavHmznJ2d1aVLF+vrTk5O8vf3l7Ozsx2rvKZMmTLy8vK64/v18fFRqVKlimy8rKwsZWdny9PTU+XKlSuycW/mTu7rdhB4AQBAkXN1dZW/v7/8/f0VFhamF198UcePH9epU6ck5Zwx3Lp1qywWizZv3qzIyEiVKlVK0dHR+umnn2zGnT9/vqpWrSoXFxfVrFlTy5Yts3ndYrFowYIF6tKli0qVKqXQ0FDt3r1bv/zyi1q1aqXSpUsrKipKhw8ftm5z45KGPXv2qF27dvL29panp6datmypvXv3Fuj4z58/r8cff1xlypRRQECApk2blqPPjbO2EyZMUKVKleTq6qqKFStqyJAhkq4tSTh27JiGDx9unTWXpMWLF6tcuXJav369ateuLVdXVx07dizXZQZXr17VM888o3LlysnLy0tjx46VYRg25+3GJRrlypWzzt6GhIRIkho2bCiLxaJWrVpJyrmk4fLlyxoyZIh8fX3l5uam++67T3v27LG+nt/vc1Ej8AIAgGJ17tw5vffee6pWrdotZ1LHjBmjadOm6dtvv5Wzs7P69u1rfW3t2rUaOnSonnvuOX3//fcaMGCAnnjiCW3ZssVmjFdeeUWPP/64kpKSVKtWLT3yyCMaMGCARo0apW+//VaS9Mwzz+RZw9mzZ9WnTx/t2LFDX331lapXr65OnTrp7Nmz+T7mkSNHasuWLVq7dq2++OILbd26VYmJiXn2//DDDzV9+nQtWLBAhw4d0kcffaR69epJktasWaPAwEBNmjTJOmt+3YULFxQfH6+3335bP/zwg3x9fXMdf8mSJXJ2dtbXX3+tWbNmafr06Xr77bfzfTzffPONJGnTpk1KSUnRmjVrcu33wgsvaPXq1VqyZIn27t2ratWqqX379vr9999t+t3s+1wsDOSQnp5uSDLS09PtXQruZZL9HzeRlpaze1raHTo3wD3g4sWLxoEDB4yLFy/mfNFBfg/kpU+fPoaTk5NRunRpo3Tp0oYkIyAgwEhMTLT2OXLkiCHJ2Ldvn2EYhrFlyxZDkrFp0yZrn08//dSQZD0H0dHRxpNPPmmzr3/84x9Gp06d/nJqZIwdO9b6fPfu3YYk45133rG2rVixwnBzc7M+Hz9+vNGgQYM8j+fq1atG2bJljU8++cRmP2vXrs21/9mzZw0XFxdj5cqV1rYzZ84Y7u7uxtChQ61twcHBxvTp0w3DMIxp06YZNWrUMDIzM3Md8699r1u0aJEhyUhKSrJp79Onj9GtWzfr85YtWxqhoaFGdna2te3FF180QkNDb3o8np6exqJFiwzDyPn9ym1f586dM0qWLGm899571tczMzONihUrGlOmTDEMI3/f57+62c9BQfIaM7wAAKDItW7dWklJSUpKStLXX3+tmJgYdezYUceOHbvpdvXr17d+HRAQIElKS0uTJB08eFDNmjWz6d+sWTMdPHgwzzH8/PwkyTpber3t0qVLysjIyLWGtLQ0DRw4UDVq1JCnp6c8PT117tw5JScn3+qwJUmHDx9WZmamoqKirG0VKlRQzZo189zmH//4hy5evKgqVaroySef1Nq1a3X16tVb7svFxcXmePPStGlT61IISYqKitKhQ4eUlZV1y23z6/Dhw7py5YrN96hkyZJq3LjxTb9HN36fiwOBFwAAFLnSpUurWrVqqlatmho3bqx33nlH58+f11tvvXXT7UqWLGn9+npAy87OztF2nWEYOdpyG+NW4/5VXFycEhMTNWPGDO3atUtJSUny8vJSZmbmTWv/a00FFRQUpJ9++klz586Vu7u7Bg0apBYtWujKlSs33c7d3T3H8ReGxWLJUfet9n2j69sX9nuU1/ejKBB4AQBAsbNYLCpRooQuXrxY6DFCQ0O1c+dOm7Zdu3YpNDT0dsuzsWPHDg0ZMkSdOnVSnTp15OrqqtOnT+d7+2rVqqlkyZL66quvrG1//PGHfv7555tu5+7urvvvv1+zZs3S1q1btXv3bv33v/+VdG0m93ZmY/9ay/Xn1atXl5OTk6RrV4z469rgQ4cO6cKFC9bnLi4uknTTGqpVqyYXFxeb79GVK1f07bffFvn3qKDsfy0QAABgOpcvX1Zqaqqka2Fvzpw5OnfunLp27VroMUeOHKmePXsqPDxcbdq00SeffKI1a9Zo06ZNRVW2pGvBbdmyZYqMjFRGRoZGjhwpd3f3fG9fpkwZ9evXTyNHjpSXl5f8/Pw0ZswYlSiR9zzj4sWLlZWVpSZNmqhUqVJatmyZ3N3dFRwcLOnaFR22b9+uXr16ydXVVd7e3gU6puPHj2vEiBEaMGCA9u7dq9mzZ9tcOeLvf/+75syZo6ZNmyo7O1svvviizSysr6+v3N3d9fnnnyswMFBubm7y9PS02Ufp0qX19NNPa+TIkapQoYIqVaqkKVOm6MKFC+rXr1+B6i1qzPACAHC3uZMfWyukzz//XAEBAQoICFCTJk20Z88effDBB9bLWRVG9+7dNXPmTL3++uuqU6eOFixYoEWLFt3WmLl599139ccff6hhw4bq3bu39TJbBfH666+rRYsWuv/++9W2bVvdd999ioiIyLN/uXLl9NZbb6lZs2aqX7++Nm/erE8++cR6VYtJkybp6NGjqlq1qnx8fAp8TI8//rguXryoxo0ba/DgwXr22Wdtbnoxbdo0BQUFqUWLFnrkkUf0/PPP21wj2NnZWbNmzdKCBQtUsWJFdevWLdf9vPbaa+rRo4d69+6t8PBw/fLLL9q4caPKly9f4JqLksUozEITk8vIyJCnp6fS09Pl4eFh73JwryrGW3rm201+PZw6Jd34+z8tTSrE72EAubh06ZKOHDmikJAQubm52bscwC5u9nNQkLzGDC8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAA6Mz5bjXlZU738CLwAADuj6NVD/evF/4F5z/e5212+QUVjceAIAAAfk5OSkcuXKKS0tTZJUqlSpIrmFLHC3yM7O1qlTp1SqVCk5O99eZCXwAgDgoPz9/SXJGnqBe02JEiVUqVKl2/7PHoEXAAAHZbFYFBAQIF9fX125csXe5QB3nIuLy01vyZxfBF4AAByck5PTba9hBO5lfGgNAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGjeeAODYbvN2kkXGMOxdAQCgkJjhBQAAgKnZPfDOmzdPISEhcnNzU0REhHbs2JFn3zVr1qhdu3by8fGRh4eHoqKitHHjRps+ixcvlsViyfG4dOlScR8KAAAAHJBdA++qVas0bNgwjRkzRvv27VPz5s3VsWNHJScn59p/+/btateunTZs2KDExES1bt1aXbt21b59+2z6eXh4KCUlxebh5uZ2Jw4JAAAADsZiGPZbmNakSROFh4dr/vz51rbQ0FB1795d8fHx+RqjTp06io2N1csvvyzp2gzvsGHD9Oeffxa6royMDHl6eio9PV0eHh6FHge4LY6wdvUmvx5OnZJ8fW3b0tIkH58irsERzoPEGl4AcDAFyWt2m+HNzMxUYmKiYmJibNpjYmK0a9eufI2RnZ2ts2fPqkKFCjbt586dU3BwsAIDA9WlS5ccM8A3unz5sjIyMmweAAAAMAe7Bd7Tp08rKytLfn5+Nu1+fn5KTU3N1xjTpk3T+fPn1bNnT2tbrVq1tHjxYq1bt04rVqyQm5ubmjVrpkOHDuU5Tnx8vDw9Pa2PoKCgwh0UAAAAHI7dP7RmueHPlYZh5GjLzYoVKzRhwgStWrVKvn/5u2rTpk312GOPqUGDBmrevLnef/991ahRQ7Nnz85zrFGjRik9Pd36OH78eOEPCAAAAA7Fbtfh9fb2lpOTU47Z3LS0tByzvjdatWqV+vXrpw8++EBt27a9ad8SJUqoUaNGN53hdXV1laura/6LBwAAwF3DbjO8Li4uioiIUEJCgk17QkKCoqOj89xuxYoViouL0/Lly9W5c+db7scwDCUlJSkgIOC2awYAAMDdx653WhsxYoR69+6tyMhIRUVFaeHChUpOTtbAgQMlXVtqcOLECS1dulTStbD7+OOPa+bMmWratKl1dtjd3V2enp6SpIkTJ6pp06aqXr26MjIyNGvWLCUlJWnu3Ln2OUgAAADYlV0Db2xsrM6cOaNJkyYpJSVFdevW1YYNGxQcHCxJSklJsbkm74IFC3T16lUNHjxYgwcPtrb36dNHixcvliT9+eefeuqpp5SamipPT081bNhQ27dvV+PGje/osQEAAMAx2PU6vI6K6/DCITjC9We5Du//4VclADiUu+I6vAAAAMCdQOAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGp2D7zz5s1TSEiI3NzcFBERoR07duTZd82aNWrXrp18fHzk4eGhqKgobdy4MUe/1atXq3bt2nJ1dVXt2rW1du3a4jwEAAAAODC7Bt5Vq1Zp2LBhGjNmjPbt26fmzZurY8eOSk5OzrX/9u3b1a5dO23YsEGJiYlq3bq1unbtqn379ln77N69W7Gxserdu7f279+v3r17q2fPnvr666/v1GEBAADAgVgMwzDstfMmTZooPDxc8+fPt7aFhoaqe/fuio+Pz9cYderUUWxsrF5++WVJUmxsrDIyMvTZZ59Z+3To0EHly5fXihUr8jVmRkaGPD09lZ6eLg8PjwIcEVCELBZ7VyDd5NfDqVOSr69tW1qa5ONTxDU4wnmQbnouAAB3XkHymt1meDMzM5WYmKiYmBib9piYGO3atStfY2RnZ+vs2bOqUKGCtW337t05xmzfvv1Nx7x8+bIyMjJsHgAAADAHuwXe06dPKysrS35+fjbtfn5+Sk1NzdcY06ZN0/nz59WzZ09rW2pqaoHHjI+Pl6enp/URFBRUgCMBAACAI7P7h9YsN/y50jCMHG25WbFihSZMmKBVq1bJ94a/qxZ0zFGjRik9Pd36OH78eAGOAAAAAI7M2V479vb2lpOTU46Z17S0tBwztDdatWqV+vXrpw8++EBt27a1ec3f37/AY7q6usrV1bWARwAAAIC7gd1meF1cXBQREaGEhASb9oSEBEVHR+e53YoVKxQXF6fly5erc+fOOV6PiorKMeYXX3xx0zHhYCwW+z8AAIBp2G2GV5JGjBih3r17KzIyUlFRUVq4cKGSk5M1cOBASdeWGpw4cUJLly6VdC3sPv7445o5c6aaNm1qncl1d3eXp6enJGno0KFq0aKFJk+erG7duunjjz/Wpk2btHPnTvscJAAAAOzKrmt4Y2NjNWPGDE2aNElhYWHavn27NmzYoODgYElSSkqKzTV5FyxYoKtXr2rw4MEKCAiwPoYOHWrtEx0drZUrV2rRokWqX7++Fi9erFWrVqlJkyZ3/PgAAABgf3a9Dq+j4jq8duYISwoc4cfCwc8D1+EFANjTXXEdXgAAAOBOIPACAADA1Ai8AAAAMDUCLwAAAEyNwAsAAABTI/ACAADA1Ai8AAAAMDUCLwAAAEyNwAsAAABTI/ACAADA1Ai8AAAAMDUCLwAAAEyNwAsAAABTI/ACAADA1Ai8AAAAMDUCLwAAAEyNwAsAAABTI/ACAADA1Ai8AAAAMDUCLwAAAEyNwAsAAABTI/ACAADA1Ai8AAAAMDUCLwAAAEyNwAsAAABTK1TgXbx4sS5cuFDUtQAAAABFrlCBd9SoUfL391e/fv20a9euoq4JAAAAKDKFCrz/+9//9O9//1t//PGHWrdurVq1amny5MlKTU0t6voAAACA21KowOvk5KT7779fa9as0fHjx/XUU0/pvffeU6VKlXT//ffr448/VnZ2dlHXCgAAABTYbX9ozdfXV82aNVNUVJRKlCih//73v4qLi1PVqlW1devWIigRAAAAKLxCB97ffvtNU6dOVZ06ddSqVStlZGRo/fr1OnLkiE6ePKkHH3xQffr0KcpaAQAAgAKzGIZhFHSjrl27auPGjapRo4b69++vxx9/XBUqVLDpc/LkSQUGBt6VSxsyMjLk6emp9PR0eXh42Luce4/FYu8KpIL/WBQ9Bz8Pp05Jvr62bWlpko9PEdfgCOdBcoz3BADAqiB5zbkwO/D19dW2bdsUFRWVZ5+AgAAdOXKkMMMDAAAARaZQSxpatmyp8PDwHO2ZmZlaunSpJMlisSg4OPj2qgMAAABuU6EC7xNPPKH09PQc7WfPntUTTzxx20UBAAAARaVQgdcwDFlyWVf3v//9T56enrddFAAAAFBUCrSGt2HDhrJYLLJYLGrTpo2cnf9v86ysLB05ckQdOnQo8iIBAACAwipQ4O3evbskKSkpSe3bt1eZMmWsr7m4uKhy5crq0aNHkRYIAAAA3I4CBd7x48dLkipXrqzY2Fi5ubkVS1EAAABAUSnUZcm4oQQAAADuFvkOvBUqVNDPP/8sb29vlS9fPtcPrV33+++/F0lxAAAAwO3Kd+CdPn26ypYta/36ZoEXAAAAcBSFurWw2XFrYTtzhP9MOcKPhYOfB24tDACwp2K5tXBGRka+CyAkAgAAwFHkO/CWK1fulssYrt+QIisr67YLAwAAAIpCvgPvli1birMOAAAAoFjkO/C2bNmyOOsAAAAAikW+A+93332nunXrqkSJEvruu+9u2rd+/fq3XRgAAABQFPIdeMPCwpSamipfX1+FhYXJYrEotws8sIYXAAAAjiTfgffIkSPy+f/XGzpy5EixFQQAAAAUpXwH3uDg4Fy/BgAAABxZvgPvjX766SfNnj1bBw8elMViUa1atfTss8+qZs2aRVkfAAAAcFtKFGajDz/8UHXr1lViYqIaNGig+vXra+/evapbt64++OCDoq4RAAAAKLRC3Vq4SpUqeuyxxzRp0iSb9vHjx2vZsmX69ddfi6xAe+DWwnbmCLeSdYTbyDr4eeDWwgAAeypIXivUDG9qaqoef/zxHO2PPfaYUlNTCzMkAAAAUCwKFXhbtWqlHTt25GjfuXOnmjdvfttFAQAAAEUl3x9aW7dunfXr+++/Xy+++KISExPVtGlTSdJXX32lDz74QBMnTiz6KgEAAIBCyvca3hIl8jcZbIYbT7CG184cYc2mI6zXdPDzwBpeAIA9FSSv5XuGNzs7+7YLAwAAAO60Qq3hBQAAAO4Whb7xxPnz57Vt2zYlJycrMzPT5rUhQ4bcdmEAAABAUShU4N23b586deqkCxcu6Pz586pQoYJOnz6tUqVKydfXl8ALAAAAh1GoJQ3Dhw9X165d9fvvv8vd3V1fffWVjh07poiICE2dOrWoawQAAAAKrVCBNykpSc8995ycnJzk5OSky5cvKygoSFOmTNHo0aMLNNa8efMUEhIiNzc3RURE5Hp93+tSUlL0yCOPqGbNmipRooSGDRuWo8/ixYtlsVhyPC5dulTQwwQAAIAJFCrwlixZUpb/f6kgPz8/JScnS5I8PT2tX+fHqlWrNGzYMI0ZM0b79u1T8+bN1bFjxzzHuHz5snx8fDRmzBg1aNAgz3E9PDyUkpJi83BzcyvAEQIAAMAsCrWGt2HDhvr2229Vo0YNtW7dWi+//LJOnz6tZcuWqV69evke54033lC/fv3Uv39/SdKMGTO0ceNGzZ8/X/Hx8Tn6V65cWTNnzpQkvfvuu3mOa7FY5O/vX8CjAgAAgBkVaob31VdfVUBAgCTplVdekZeXl55++mmlpaVp4cKF+RojMzNTiYmJiomJsWmPiYnRrl27ClOW1blz5xQcHKzAwEB16dJF+/btu2n/y5cvKyMjw+YBAAAAcyjUDG9kZKT1ax8fH23YsKHAY5w+fVpZWVny8/Ozaffz81NqamphypIk1apVS4sXL1a9evWUkZGhmTNnqlmzZtq/f7+qV6+e6zbx8fHcEtkBvLLtFf1zxz+lsfauRNI/Xe1dgeOfB0M5agx8U1JR3xjNEc6D5BjviWIytvlYjWs5zt5lAECxKfR1eCUpLS1NP/30kywWi2rWrCmfQtxT1HLDbUMNw8jRVhBNmzZV06ZNrc+bNWum8PBwzZ49W7Nmzcp1m1GjRmnEiBHW5xkZGQoKCip0DSicLCNLmVmZt/muLCJZmbfuU9zuhvNwQ42ZxXFDRkc4D5JjvCeKSZZxd98OHgBupVD/lGRkZGjw4MFauXKlsrKu/aJ0cnJSbGys5s6dK09Pz1uO4e3tLScnpxyzuWlpaTlmfW9HiRIl1KhRIx06dCjPPq6urnJ1Ne/sDQAAwL2sUGt4+/fvr6+//lrr16/Xn3/+qfT0dK1fv17ffvutnnzyyXyN4eLiooiICCUkJNi0JyQkKDo6ujBl5cowDCUlJVnXHAMAAODeUqgZ3k8//VQbN27UfffdZ21r37693nrrLXXo0CHf44wYMUK9e/dWZGSkoqKitHDhQiUnJ2vgwIGSri01OHHihJYuXWrdJikpSdK1D6adOnVKSUlJcnFxUe3atSVJEydOVNOmTVW9enVlZGRo1qxZSkpK0ty5cwtzqAAAALjLFSrwenl55bpswdPTU+XLl8/3OLGxsTpz5owmTZqklJQU1a1bVxs2bFBwcLCkazeauPGavA0bNrR+nZiYqOXLlys4OFhHjx6VJP3555966qmnlJqaKk9PTzVs2FDbt29X48aNC3GkAAAAuNtZDMMwCrrRwoUL9cEHH2jp0qXWpQKpqanq06ePHnzwQQ0YMKDIC72TMjIy5OnpqfT0dHl4eNi7nHuG9SoNlx3gw0GuLvauwPHPgyFl3lCii4uK/ioNjnAeJMd4TxQTrtIA4G5UkLyW78DbsGFDm6snHDp0SJcvX1alSpUkScnJyXJ1dVX16tW1d+/e2yjf/gi8dnYbV+koMgX/f2DRc/DzcOqU5Otr25aWJhXiYi035wjnQXKM9wQAwKogeS3fSxq6d+9+u3UBAAAAd1y+A+/48eOLsw4AAACgWNzWJd0TExN18OBBWSwW1a5d2+YDZQAAAIAjKFTgTUtLU69evbR161aVK1dOhmEoPT1drVu31sqVKwt1xzUAAACgOBTqxhPPPvusMjIy9MMPP+j333/XH3/8oe+//14ZGRkaMmRIUdcIAAAAFFqhZng///xzbdq0SaGhoda22rVra+7cuYqJiSmy4gAAAIDbVagZ3uzsbJUsWTJHe8mSJZWdnX3bRQEAAABFpVCB9+9//7uGDh2qkydPWttOnDih4cOHq02bNkVWHAAAAHC7ChV458yZo7Nnz6py5cqqWrWqqlWrppCQEJ09e1azZ88u6hoBAACAQivUGt6goCDt3btXCQkJ+vHHH2UYhmrXrq22bdsWdX0AAADAbSlw4L169arc3NyUlJSkdu3aqV27dsVRFwAAAFAkCrykwdnZWcHBwcrKyiqOegAAAIAiVag1vGPHjtWoUaP0+++/F3U9AAAAQJEq1BreWbNm6ZdfflHFihUVHBys0qVL27y+d+/eIikOAPAXFou9K5AMw94VAECBFSrwdu/eXRaLRQa/+AAAAODgChR4L1y4oJEjR+qjjz7SlStX1KZNG82ePVve3t7FVR8AAABwWwq0hnf8+PFavHixOnfurIcfflibNm3S008/XVy1AQAAALetQDO8a9as0TvvvKNevXpJkh599FE1a9ZMWVlZcnJyKpYCAQAAgNtRoBne48ePq3nz5tbnjRs3lrOzs80thgEAAABHUqDAm5WVJRcXF5s2Z2dnXb16tUiLAgAAAIpKgZY0GIahuLg4ubq6WtsuXbqkgQMH2lyabM2aNUVXIQAAAHAbChR4+/Tpk6PtscceK7JiAAAAgKJWoMC7aNGi4qoDAAAAKBaFurUwAAAAcLcg8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNbsH3nnz5ikkJERubm6KiIjQjh078uybkpKiRx55RDVr1lSJEiU0bNiwXPutXr1atWvXlqurq2rXrq21a9cWU/UAAABwdHYNvKtWrdKwYcM0ZswY7du3T82bN1fHjh2VnJyca//Lly/Lx8dHY8aMUYMGDXLts3v3bsXGxqp3797av3+/evfurZ49e+rrr78uzkMBAACAg7IYhmHYa+dNmjRReHi45s+fb20LDQ1V9+7dFR8ff9NtW7VqpbCwMM2YMcOmPTY2VhkZGfrss8+sbR06dFD58uW1YsWKfNWVkZEhT09Ppaeny8PDI/8HhKJhsdi7Asl+Pxb/x8HPw6lTkq+vbVtamuTjU8Q1OMJ5kHhPXOcI5wEAVLC8ZrcZ3szMTCUmJiomJsamPSYmRrt27Sr0uLt3784xZvv27W865uXLl5WRkWHzAAAAgDnYLfCePn1aWVlZ8vPzs2n38/NTampqocdNTU0t8Jjx8fHy9PS0PoKCggq9fwAAADgWu39ozXLDn+gMw8jRVtxjjho1Sunp6dbH8ePHb2v/AAAAcBzO9tqxt7e3nJyccsy8pqWl5ZihLQh/f/8Cj+nq6ipXV9dC7xMAAACOy24zvC4uLoqIiFBCQoJNe0JCgqKjows9blRUVI4xv/jii9saEwAAAHcvu83wStKIESPUu3dvRUZGKioqSgsXLlRycrIGDhwo6dpSgxMnTmjp0qXWbZKSkiRJ586d06lTp5SUlCQXFxfVrl1bkjR06FC1aNFCkydPVrdu3fTxxx9r06ZN2rlz5x0/PgAAANifXQNvbGyszpw5o0mTJiklJUV169bVhg0bFBwcLOnajSZuvCZvw4YNrV8nJiZq+fLlCg4O1tGjRyVJ0dHRWrlypcaOHatx48apatWqWrVqlZo0aXLHjgsAAACOw67X4XVUXIfXzrjW6DUOfh64Dq8dOMK5cITzAAC6S67DCwAAANwJBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKk527sA/H8Wi70ruMYw7F0BAABAkWKGFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABganYPvPPmzVNISIjc3NwUERGhHTt23LT/tm3bFBERITc3N1WpUkVvvvmmzeuLFy+WxWLJ8bh06VJxHgYAAAAclF0D76pVqzRs2DCNGTNG+/btU/PmzdWxY0clJyfn2v/IkSPq1KmTmjdvrn379mn06NEaMmSIVq9ebdPPw8NDKSkpNg83N7c7cUgAAABwMHa909obb7yhfv36qX///pKkGTNmaOPGjZo/f77i4+Nz9H/zzTdVqVIlzZgxQ5IUGhqqb7/9VlOnTlWPHj2s/SwWi/z9/e/IMQAAAMCx2W2GNzMzU4mJiYqJibFpj4mJ0a5du3LdZvfu3Tn6t2/fXt9++62uXLlibTt37pyCg4MVGBioLl26aN++fTet5fLly8rIyLB5AAAAwBzsFnhPnz6trKws+fn52bT7+fkpNTU1121SU1Nz7X/16lWdPn1aklSrVi0tXrxY69at04oVK+Tm5qZmzZrp0KFDedYSHx8vT09P6yMoKOg2jw4AAACOwu4fWrNYLDbPDcPI0Xar/n9tb9q0qR577DE1aNBAzZs31/vvv68aNWpo9uzZeY45atQopaenWx/Hjx8v7OEAAADAwdhtDa+3t7ecnJxyzOampaXlmMW9zt/fP9f+zs7O8vLyynWbEiVKqFGjRjed4XV1dZWrq2sBjwAAAAB3A7vN8Lq4uCgiIkIJCQk27QkJCYqOjs51m6ioqBz9v/jiC0VGRqpkyZK5bmMYhpKSkhQQEFA0hQMAAOCuYterNIwYMUK9e/dWZGSkoqKitHDhQiUnJ2vgwIGSri01OHHihJYuXSpJGjhwoObMmaMRI0boySef1O7du/XOO+9oxYoV1jEnTpyopk2bqnr16srIyNCsWbOUlJSkuXPn2uUYAQBF7CbL3u6Y/7+cDsDdwa6BNzY2VmfOnNGkSZOUkpKiunXrasOGDQoODpYkpaSk2FyTNyQkRBs2bNDw4cM1d+5cVaxYUbNmzbK5JNmff/6pp556SqmpqfL09FTDhg21fft2NW7c+I4fHwAAAOzPYhj8N/VGGRkZ8vT0VHp6ujw8PO7MTh1hxkJyjFkLRzgXnIdrbnIeTp2SfH1t29LSJB+fIq7BEc6DxHviOs7DNY5wHoB7XEHymt2v0gAAAAAUJwIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUnO1dAAAAKASLxd4VSIZh7wqAfGGGFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICp2T3wzps3TyEhIXJzc1NERIR27Nhx0/7btm1TRESE3NzcVKVKFb355ps5+qxevVq1a9eWq6urateurbVr1xZX+QAAAHBwdg28q1at0rBhwzRmzBjt27dPzZs3V8eOHZWcnJxr/yNHjqhTp05q3ry59u3bp9GjR2vIkCFavXq1tc/u3bsVGxur3r17a//+/erdu7d69uypr7/++k4dFgAAAByIxTAMw147b9KkicLDwzV//nxrW2hoqLp37674+Pgc/V988UWtW7dOBw8etLYNHDhQ+/fv1+7duyVJsbGxysjI0GeffWbt06FDB5UvX14rVqzIV10ZGRny9PRUenq6PDw8Cnt4BWOx3Jn93Ir93g7/xxHOBefhmpuch1OnJF9f27a0NMnHp4hrcITzIPGeuI7zcA3n4RpHOA8S5+IeVZC85nyHasohMzNTiYmJeumll2zaY2JitGvXrly32b17t2JiYmza2rdvr3feeUdXrlxRyZIltXv3bg0fPjxHnxkzZuRZy+XLl3X58mXr8/T0dEnXTuQ951485txwHq65yXk4ezb3NlfXYqzHnnhPXMN5uIbzcA3n4f9wLu646zktP3O3dgu8p0+fVlZWlvz8/Gza/fz8lJqamus2qampufa/evWqTp8+rYCAgDz75DWmJMXHx2vixIk52oOCgvJ7OObh6WnvChwD5+GaAp6HqlWLqQ5HwHviGs7DNZyHazgP/4dzYTdnz56V5y3Ov90C73WWG/4MYRhGjrZb9b+xvaBjjho1SiNGjLA+z87O1u+//y4vL6+bbudIMjIyFBQUpOPHj9+5ZRhwaLwn8Fe8H3Aj3hO40d32njAMQ2fPnlXFihVv2ddugdfb21tOTk45Zl7T0tJyzNBe5+/vn2t/Z2dneXl53bRPXmNKkqurq1xv+DtsuXLl8nsoDsXDw+OueJPizuE9gb/i/YAb8Z7Aje6m98StZnavs9tVGlxcXBQREaGEhASb9oSEBEVHR+e6TVRUVI7+X3zxhSIjI1WyZMmb9slrTAAAAJibXZc0jBgxQr1791ZkZKSioqK0cOFCJScna+DAgZKuLTU4ceKEli5dKunaFRnmzJmjESNG6Mknn9Tu3bv1zjvv2Fx9YejQoWrRooUmT56sbt266eOPP9amTZu0c+dOuxwjAAAA7MuugTc2NlZnzpzRpEmTlJKSorp162rDhg0KDg6WJKWkpNhckzckJEQbNmzQ8OHDNXfuXFWsWFGzZs1Sjx49rH2io6O1cuVKjR07VuPGjVPVqlW1atUqNWnS5I4f353k6uqq8ePH51iagXsX7wn8Fe8H3Ij3BG5k5veEXa/DCwAAABQ3u99aGAAAAChOBF4AAACYGoEXAAAApkbgBQAAgKkReE1g3rx5CgkJkZubmyIiIrRjxw57lwQ7iY+PV6NGjVS2bFn5+vqqe/fu+umnn+xdFhxIfHy8LBaLhg0bZu9SYCcnTpzQY489Ji8vL5UqVUphYWFKTEy0d1mwk6tXr2rs2LEKCQmRu7u7qlSpokmTJik7O9vepRUpAu9dbtWqVRo2bJjGjBmjffv2qXnz5urYsaPN5dxw79i2bZsGDx6sr776SgkJCbp69apiYmJ0/vx5e5cGB7Bnzx4tXLhQ9evXt3cpsJM//vhDzZo1U8mSJfXZZ5/pwIEDmjZt2l17d1HcvsmTJ+vNN9/UnDlzdPDgQU2ZMkWvv/66Zs+ebe/SihSXJbvLNWnSROHh4Zo/f761LTQ0VN27d1d8fLwdK4MjOHXqlHx9fbVt2za1aNHC3uXAjs6dO6fw8HDNmzdP//znPxUWFqYZM2bYuyzcYS+99JL+85//8JdAWHXp0kV+fn565513rG09evRQqVKltGzZMjtWVrSY4b2LZWZmKjExUTExMTbtMTEx2rVrl52qgiNJT0+XJFWoUMHOlcDeBg8erM6dO6tt27b2LgV2tG7dOkVGRuof//iHfH191bBhQ7311lv2Lgt2dN9992nz5s36+eefJUn79+/Xzp071alTJztXVrTseqc13J7Tp08rKytLfn5+Nu1+fn5KTU21U1VwFIZhaMSIEbrvvvtUt25de5cDO1q5cqX27t2rPXv22LsU2Nmvv/6q+fPna8SIERo9erS++eYbDRkyRK6urnr88cftXR7s4MUXX1R6erpq1aolJycnZWVl6V//+pcefvhhe5dWpAi8JmCxWGyeG4aRow33nmeeeUbfffeddu7cae9SYEfHjx/X0KFD9cUXX8jNzc3e5cDOsrOzFRkZqVdffVWS1LBhQ/3www+aP38+gfcetWrVKv373//W8uXLVadOHSUlJWnYsGGqWLGi+vTpY+/yigyB9y7m7e0tJyenHLO5aWlpOWZ9cW959tlntW7dOm3fvl2BgYH2Lgd2lJiYqLS0NEVERFjbsrKytH37ds2ZM0eXL1+Wk5OTHSvEnRQQEKDatWvbtIWGhmr16tV2qgj2NnLkSL300kvq1auXJKlevXo6duyY4uPjTRV4WcN7F3NxcVFERIQSEhJs2hMSEhQdHW2nqmBPhmHomWee0Zo1a/Tll18qJCTE3iXBztq0aaP//ve/SkpKsj4iIyP16KOPKikpibB7j2nWrFmOSxX+/PPPCg4OtlNFsLcLFy6oRAnbOOjk5GS6y5Ixw3uXGzFihHr37q3IyEhFRUVp4cKFSk5O1sCBA+1dGuxg8ODBWr58uT7++GOVLVvWOvvv6ekpd3d3O1cHeyhbtmyONdylS5eWl5cXa7vvQcOHD1d0dLReffVV9ezZU998840WLlyohQsX2rs02EnXrl31r3/9S5UqVVKdOnW0b98+vfHGG+rbt6+9SytSXJbMBObNm6cpU6YoJSVFdevW1fTp07kE1T0qr7XbixYtUlxc3J0tBg6rVatWXJbsHrZ+/XqNGjVKhw4dUkhIiEaMGKEnn3zS3mXBTs6ePatx48Zp7dq1SktLU8WKFfXwww/r5ZdflouLi73LKzIEXgAAAJgaa3gBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBoJgdPXpUFotFSUlJ9i7F6scff1TTpk3l5uamsLAwe5cDAMWKwAvA9OLi4mSxWPTaa6/ZtH/00Ud53o7Z7MaPH6/SpUvrp59+0ubNm+1dzk21atVKw4YNs3cZAO5iBF4A9wQ3NzdNnjxZf/zxh71LKTKZmZmF3vbw4cO67777FBwcLC8vryKs6u5wO+cOwN2HwAvgntC2bVv5+/srPj4+zz4TJkzI8ef9GTNmqHLlytbncXFx6t69u1599VX5+fmpXLlymjhxoq5evaqRI0eqQoUKCgwM1Lvvvptj/B9//FHR0dFyc3NTnTp1tHXrVpvXDxw4oE6dOqlMmTLy8/NT7969dfr0aevrrVq10jPPPKMRI0bI29tb7dq1y/U4srOzNWnSJAUGBsrV1VVhYWH6/PPPra9bLBYlJiZq0qRJslgsmjBhQq7jGIahKVOmqEqVKnJ3d1eDBg304YcfWvcRGBioN99802abvXv3ymKx6Ndff5Ukpaen66mnnpKvr688PDz097//Xfv3789xzpctW6bKlSvL09NTvXr10tmzZ63ne9u2bZo5c6YsFossFouOHj2qP/74Q48++qh8fHzk7u6u6tWra9GiRbkeR0HOHQBzIvACuCc4OTnp1Vdf1ezZs/W///3vtsb68ssvdfLkSW3fvl1vvPGGJkyYoC5duqh8+fL6+uuvNXDgQA0cOFDHjx+32W7kyJF67rnntG/fPkVHR+v+++/XmTNnJEkpKSlq2bKlwsLC9O233+rzzz/Xb7/9pp49e9qMsWTJEjk7O+s///mPFixYkGt9M2fO1LRp0zR16lR99913at++ve6//34dOnTIuq86deroueeeU0pKip5//vlcxxk7dqwWLVqk+fPn64cfftDw4cP12GOPadu2bSpRooR69eql9957z2ab5cuXKyoqSlWqVJFhGOrcubNSU1O1YcMGJSYmKjw8XG3atNHvv/9u3ebw4cP66KOPtH79eq1fv17btm2zLj+ZOXOmoqKi9OSTTyolJUUpKSkKCgrSuHHjdODAAX322Wc6ePCg5s+fL29v75t+3/Jz7gCYlAEAJtenTx+jW7duhmEYRtOmTY2+ffsahmEYa9euNf76a3D8+PFGgwYNbLadPn26ERwcbDNWcHCwkZWVZW2rWbOm0bx5c+vzq1evGqVLlzZWrFhhGIZhHDlyxJBkvPbaa9Y+V65cMQIDA43JkycbhmEY48aNM2JiYmz2ffz4cUOS8dNPPxmGYRgtW7Y0wsLCbnm8FStWNP71r3/ZtDVq1MgYNGiQ9XmDBg2M8ePH5znGuXPnDDc3N2PXrl027f369TMefvhhwzAMY+/evYbFYjGOHj1qGIZhZGVlGX/729+MuXPnGoZhGJs3bzY8PDyMS5cu2YxRtWpVY8GCBYZhXDvnpUqVMjIyMqyvjxw50mjSpIn1ecuWLY2hQ4fajNG1a1fjiSeeuNlpsJHfcwfAnJjhBXBPmTx5spYsWaIDBw4Ueow6deqoRIn/+/Xp5+enevXqWZ87OTnJy8tLaWlpNttFRUVZv3Z2dlZkZKQOHjwoSUpMTNSWLVtUpkwZ66NWrVqSrs2AXhcZGXnT2jIyMnTy5Ek1a9bMpr1Zs2bWfeXHgQMHdOnSJbVr186mpqVLl1rradiwoWrVqqUVK1ZIkrZt26a0tDTrrHRiYqLOnTsnLy8vmzGOHDlic0yVK1dW2bJlrc8DAgJynLsbPf3001q5cqXCwsL0wgsvaNeuXbc8pludOwDm5WzvAgDgTmrRooXat2+v0aNHKy4uzua1EiVKyDAMm7YrV67kGKNkyZI2zy0WS65t2dnZt6zn+lUisrOz1bVrV02ePDlHn4CAAOvXpUuXvuWYfx33OsMwCnRFiuu1f/rpp/rb3/5m85qrq6v160cffVTLly/XSy+9pOXLl6t9+/bWpQXZ2dkKCAjIsVZZksqVK2f9ujDnrmPHjjp27Jg+/fRTbdq0SW3atNHgwYM1derUPLfJ77kDYD4EXgD3nNdee01hYWGqUaOGTbuPj49SU1NtwmFRXjv3q6++UosWLSRJV69eVWJiop555hlJUnh4uFavXq3KlSvL2bnwv5o9PDxUsWJF7dy507ovSdq1a5caN26c73Fq164tV1dXJScnq2XLlnn2e+SRRzR27FglJibqww8/1Pz5862vhYeHKzU1Vc7OzjYf/CsoFxcXZWVl5Wj38fFRXFyc4uLi1Lx5c40cOfKmgRfAvYvAC+CeU69ePT366KOaPXu2TXurVq106tQpTZkyRQ899JA+//xzffbZZ/Lw8CiS/c6dO1fVq1dXaGiopk+frj/++EN9+/aVJA0ePFhvvfWWHn74YY0cOVLe3t765ZdftHLlSr311ltycnLK935Gjhyp8ePHq2rVqgoLC9OiRYuUlJSU4wNmN1O2bFk9//zzGj58uLKzs3XfffcpIyNDu3btUpkyZdSnTx9JUkhIiKKjo9WvXz9dvXpV3bp1s47Rtm1bRUVFqXv37po8ebJq1qypkydPasOGDerevXu+lxhUrlxZX3/9tY4ePaoyZcqoQoUKmjBhgiIiIlSnTh1dvnxZ69evV2hoaL6PD8C9hTW8AO5Jr7zySo7lC6GhoZo3b57mzp2rBg0a6JtvvsnzCgaF8dprr2ny5Mlq0KCBduzYoY8//tj65/+KFSvqP//5j7KystS+fXvVrVtXQ4cOlaenp8164fwYMmSInnvuOT333HOqV6+ePv/8c61bt07Vq1cv0DivvPKKXn75ZcXHxys0NFTt27fXJ598opCQEJt+jz76qPbv368HH3xQ7u7u1naLxaINGzaoRYsW6tu3r2rUqKFevXrp6NGj8vPzy3cdzz//vJycnFS7dm35+PgoOTlZLi4uGjVqlOrXr68WLVrIyclJK1euLNDxAbh3WIwbf+MDAAAAJsIMLwAAAEyNwAsAAABTI/ACAADA1Ai8AAAAMDUCLwAAAEyNwAsAAABTI/ACAADA1Ai8AAAAMDUCLwAAAEyNwAsAAABTI/ACAADA1P4fsh0m4RPXAKYAAAAASUVORK5CYII=",
      "text/plain": [
       "<Figure size 800x600 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# calculate expected mean and standard deviation\n",
    "mu=n*p\n",
    "sig=n*p*(1.-p)\n",
    "# Create a Matplotlib figure and axis\n",
    "fig, ax = plt.subplots(figsize=(8, 6))\n",
    "\n",
    "# Plot the binomial distribution with vertical bars\n",
    "ax.bar(xr, fr, width=0.5, color='red', label='Binomial distribution')\n",
    "\n",
    "# Draw the location of the mean\n",
    "ax.axvline(mu, linewidth=3, color='blue', label='Mean')\n",
    "\n",
    "# Draw a horizontal line to represent the standard deviation\n",
    "ax.hlines(np.max(fr) / 2., mu - sig, mu + sig, linewidth=5, color='green', label='Standard Deviation')\n",
    "\n",
    "# Set labels and title\n",
    "ax.set_xlabel('Number of events r')\n",
    "ax.set_ylabel('Probability')\n",
    "ax.set_title('Binomial Distribution')\n",
    "\n",
    "# Show legend\n",
    "ax.legend()\n",
    "\n",
    "# Show the Matplotlib plot\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##Pascal's Triangle is intimately connected with the binomial distribution.\n",
    "It is simply the coefficients in the expansion:\n",
    "$$ ^nC_r= \\frac{n!}{r!(n-r!)}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "           1 \n",
      "          1 1 \n",
      "         1 2 1 \n",
      "        1 3 3 1 \n",
      "       1 4 6 4 1 \n",
      "      1 5 10 10 5 1 \n",
      "     1 6 15 20 15 6 1 \n",
      "    1 7 21 35 35 21 7 1 \n",
      "   1 8 28 56 70 56 28 8 1 \n",
      "  1 9 36 84 126 126 84 36 9 1 \n",
      "22 33\n"
     ]
    }
   ],
   "source": [
    "# Print Pascal's Triangle \n",
    " \n",
    "# set number of row\n",
    "nrow = 10\n",
    "neven=0 \n",
    "nodd=0\n",
    "for i in range(nrow):\n",
    "    for j in range(nrow-i+1):\n",
    " \n",
    "        # for left spacing\n",
    "        print(end=\" \")\n",
    " \n",
    "    for j in range(i+1):\n",
    " \n",
    "        # nCr = n!/((n-r)!*r!)\n",
    "        pterm=factorial(i)//(factorial(j)*factorial(i-j))\n",
    "        print(pterm, end=\" \")\n",
    "        if (pterm % 2) == 0: neven=neven+1\n",
    "        else: nodd=nodd+1 \n",
    "    # for new line\n",
    "    print()\n",
    "print(neven,nodd)    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
