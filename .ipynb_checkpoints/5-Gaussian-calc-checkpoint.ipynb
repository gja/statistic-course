{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gaussian calculator  \n",
    "*Version 2023 gja*\n",
    "\n",
    "The Gaussian distribution is given by\n",
    "\n",
    "$$f(x) = \\frac{1}{\\sigma\\sqrt{2\\pi}} \n",
    "\\exp{ \\left[ - \\frac{1}{2} \\left( \n",
    "\\frac{x-\\mu}{\\sigma}\n",
    "\\right)^2 \\right]}$$\n",
    "\n",
    "This is a probability density function $f(x)$, so that $f(x)dx$ is the probability of $x$ being in the range $x$ to $x+dx$, for mean $\\mu$ and dispersion $\\sigma$. Below we provide calculators for (i) the probability density at $x$ for a given $\\mu, \\sigma$; (ii) the integral probability above $x$, and (iii) the integral probability between $x_1$ and $x_2$. \n",
    "\n",
    "If you want to explore graphically how the integral behaves, try the [Gaussian Areas Notebook](Gaussian-areas.ipynb) \n",
    "\n",
    "As usual, do Cell/Run All first, then you can edit parameters for specific cells. First, a standard set-up."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import *  # basic maths routines\n",
    "import numpy as np\n",
    "from scipy import stats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (1) Probability density for a given $x,\\mu,\\sigma$\n",
    "\n",
    "In the code cell below, edit in the values of $x,\\mu,\\sigma$ you want and then run the cell. Note that we have rounded down to three decimal places, but you can change that if you wish by uncommenting the extra line. Remember that this probability density carries the units of $x$, i.e. it is the probability per unit $x$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x= 4.9  mu= 6.2  sig= 2.3   ==> f= 0.148\n"
     ]
    }
   ],
   "source": [
    "####### Gaussian calculator\n",
    "mu=6.2\n",
    "sig=2.3\n",
    "x=4.9\n",
    "######################\n",
    "f=round(stats.norm.pdf(x,mu,sig),3)\n",
    "# \"pdf\" stands for \"probability density function\"\n",
    "# note difference with discrete prob.distbns where we used\n",
    "# \"pmf\" for probability mass function.\n",
    "print('x=',x,' mu=',mu,' sig=',sig,'  ==> f=',f)\n",
    "###########"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (2) Probability above $x$ for given $\\mu, \\sigma$\n",
    "\n",
    "Below we calculate both the \"one-tailed\" and \"two-tailed\" probabilities, i.e. \n",
    "\n",
    "$$P_1=(P>x) {\\qquad\\rm and \\qquad} P_2=2P_1$$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x= 5.0  mu= 0.0   sig= 1.92 ==>  P1= 0.0046  P2= 0.0092\n"
     ]
    }
   ],
   "source": [
    "### Gaussian integral above/below x\n",
    "mu=0.0\n",
    "sig=1.92\n",
    "x=5.0\n",
    "################\n",
    "#note standard cdf is below x; we want prob above x\n",
    "fgx=stats.norm.cdf(x,mu,sig) # integral at x and below\n",
    "P1=round((1.-fgx),5)\n",
    "P2=2.*P1\n",
    "\n",
    "print('x=',x, ' mu=',mu,'  sig=',sig, '==>',\n",
    "     ' P1=',P1,' P2=',P2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (3) Probability between $x_1$ and $x_2$\n",
    "\n",
    "For most problems we want the symmetrical probabilities. But sometimes we want the area between two arbitrary values of $x_1$ and $x_2$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x1= 4.6  x2= -4.6  mu= 0.0 sig= 2.3 ==>  area between= 0.954\n"
     ]
    }
   ],
   "source": [
    "### Gaussian integral between two x-values\n",
    "mu=0.0\n",
    "sig=2.3\n",
    "x1=4.6\n",
    "x2=-4.6\n",
    "################\n",
    "fgx1=stats.norm.cdf(x1,mu,sig) # integral at x1 and below\n",
    "fgx2=stats.norm.cdf(x2,mu,sig) # integral at x2 and below\n",
    "\n",
    "between=round((fgx1-fgx2),3)\n",
    "print('x1=',x1,' x2=',x2, ' mu=',mu,'sig=',sig, '==>',\n",
    "     ' area between=',between)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (4) Value of centiles\n",
    "\n",
    "We  may want to know the value of $z$ below which X% of measurements will lie.  We need the inverse of the CDF\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "mu= 0 Sigma= 1 ;   84.0 % of the distribution lies below x= 0.994457883209753\n"
     ]
    }
   ],
   "source": [
    "X=0.84\n",
    "sig=1\n",
    "mu=0\n",
    "print(\"mu=\", mu, \"Sigma=\", sig,\";  \", X*100,\"% of the distribution lies below x=\", stats.norm.ppf(X,mu,sig))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
