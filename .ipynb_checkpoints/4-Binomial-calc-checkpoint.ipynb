{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binomial calculator  \n",
    "*Version 2023*  \n",
    "\n",
    "The binomial distribution is given by\n",
    "\n",
    "$$f_n(r) = \\frac{n!}{r!(n-r!)} p^r (1-p)^{n-r}$$\n",
    "\n",
    "This is the probability of $r$ successes out of $n$ trials, if the probability of success at each trial is $p$. Below we provide code cells to calculate (i) a single specific probabiity for a given $p,n,r$, (ii) the cumulative probability at $r$ and above for a given $n$, and (iii) the complete set of $f(r)$ values for a given $n$. Finally, we plot the $f(r)$ values.  \n",
    "\n",
    "As usual, do Cell/Run All first, then you can edit parameters for specific cells. First, standard set-up."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import *  # basic maths routines\n",
    "import numpy as np \n",
    "from scipy import stats\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (1) Single $f_n(r)$ values\n",
    "\n",
    "In the code cell below, edit in the values of $p,n,r$ you want and then run the cell. Note that we have rounded down to two decimal places, but you can change that if you wish by uncommenting the extra line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "p= 0.3 n= 6 r= 2   ==> f= 0.32\n"
     ]
    }
   ],
   "source": [
    "# Binomial calculator\n",
    "p=0.3\n",
    "n=6\n",
    "r=2\n",
    "f=stats.binom.pmf(r,n,p) # \"pmf\" stands for \"probability mass function\"\n",
    "print('p=',p,'n=',n,'r=',r,'  ==> f=',round(f,2))\n",
    "# print('p=',p,'n=',n,'r=',r,'  ==> f=',f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Its not hard to code up the binomial function yourself. Below is how you would do it. *You could try writing some extra code and comparing the results to see if you get the same answer...*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Home grown Binomial calculator : P(r,n,p)\n",
    "# prob of r successes out of n trials if expected fraction is p\n",
    "def Bin(r,n,p) :\n",
    " P = (factorial(n)/(factorial(r)*factorial(n-r))) * p**r * (1-p)**(n-r)\n",
    " return P"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (2) Cumulative $f_n(>r)$ values\n",
    "\n",
    "In the code cell below, edit in the values of $p,n,r$ you want and then run the cell. Note that we are resetting all of $p,n,r$ to make sure we don't accidentally re-use values from the first calculator. Note that we have used a precision of three decimal places, but you can change this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "r= 3   n= 8   p= 0.5\n",
      "summed prob at r and below= 0.363\n",
      "summed prob above r= 0.637\n",
      "(checksum= 1.0 )\n"
     ]
    }
   ],
   "source": [
    "# Cumulative binomial calculator\n",
    "p=0.5\n",
    "n=8\n",
    "r=3\n",
    "fle=round(stats.binom.cdf(r,n,p),3) # summed prob at r and below\n",
    "fge=round(stats.binom.sf(r,n,p),3) # summed prob above r\n",
    "checksum=fge+fle\n",
    "print('r=',r,'  n=',n,'  p=',p)\n",
    "print('summed prob at r and below=',fle) # \"cdf\"= cumulative distbn fn.\n",
    "print('summed prob above r=',fge)  # \"sf\" = survival function\n",
    "print('(checksum=',checksum,')')\n",
    "# note the \"survival function\" should be 1-cdf, but the Scipy docs\n",
    "# say that sf is sometimes more accurate\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (3) Complete set of  $f_n(r)$ values\n",
    "\n",
    "Here we set $p,n$ and then calculate $f(r)$ for all the possible $r$ values. Careful - if you set a large value of $n$ you will get a long output cell!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "p= 0.3 n= 8\n",
      "r= 0 f(r)= 0.05765\n",
      "r= 1 f(r)= 0.19765\n",
      "r= 2 f(r)= 0.29648\n",
      "r= 3 f(r)= 0.25412\n",
      "r= 4 f(r)= 0.13614\n",
      "r= 5 f(r)= 0.04668\n",
      "r= 6 f(r)= 0.01\n",
      "r= 7 f(r)= 0.00122\n",
      "r= 8 f(r)= 7e-05\n"
     ]
    }
   ],
   "source": [
    "# set parameters\n",
    "n=8 # total number of trials\n",
    "p=0.3 # probability of success per trial\n",
    "\n",
    "xr=np.arange(0,n+1) # set up array of r values\n",
    "fr=np.zeros(len(xr)) # initialise array to hold f(r) values\n",
    "\n",
    "# make fr\n",
    "for i in range(0,len(xr)):\n",
    "    fr[i]=round(stats.binom.pmf(xr[i],n,p),5)\n",
    "    \n",
    "print('p=',p,'n=',n,)\n",
    "for i in range(0,len(xr)):\n",
    "    print('r=',xr[i],'f(r)=',fr[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### (4) Plot the distribution\n",
    "\n",
    "We will mark the mean and variance as well as plotting the distribution.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAArwAAAIhCAYAAACsQmneAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjcuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8pXeV/AAAACXBIWXMAAA9hAAAPYQGoP6dpAABYG0lEQVR4nO3deVxUZf//8fcAsriACoIYiLikuKKQCuZ2q7hlWnaLZSblkqW5lZVLuXQXae5r2uJWbqWWmWloud1QGYrdpXeZqZjCjVqBOwrn94c/59sIKCA44/H1fDzm8WCuuc51Pucw8nh7zTXnWAzDMAQAAACYlJO9CwAAAACKE4EXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXgENZvHixLBaLzaNChQpq1aqVNmzYkKO/xWLR+PHjb3+hBXDtmI4cOVJs215/3tzd3VWxYkW1bt1asbGxSktLy7HN+PHjZbFYClTP+fPnNX78eG3btq1A2+W2rypVquiBBx4o0Dg3s3z5cs2YMSPX1+6E9wqA4uFi7wIAIDeLFi1SrVq1ZBiGUlNTNWfOHHXp0kXr169Xly5drP0SEhIUEBBgx0pvrnPnzkpISJC/v3+x7+vaebt8+bLS0tK0a9cuTZo0SVOmTNGqVavUtm1ba99+/fqpQ4cOBRr//PnzmjBhgiSpVatW+d6uMPsqjOXLl+vHH3/UsGHDcrx2J7xXABQPAi8Ah1S3bl2Fh4dbn3fo0EHlypXTihUrbAJv06ZN7VFegVSoUEEVKlS4Lfu6/rx1795dw4cP1/3336+HH35YBw8elJ+fnyQpICCg2APg+fPnVbJkyduyr5u5E94rAIoHSxoA3BHc3d3l6uqqEiVK2LRf/zH1tY/2v/76az3zzDPy8fGRt7e3Hn74YZ04ccJm2+zsbE2ePFm1atWSm5ubfH199cQTT+j333+36deqVSvVrVtXCQkJioyMlIeHh6pUqaJFixZJkj7//HM1atRIJUuWVL169bRp0yab7XNblhAXF6euXbsqICBA7u7uql69up5++mmdOnWqCM6WrcqVK2vq1Kk6c+aMFixYYG3PbZnBV199pVatWsnb21seHh6qXLmyunfvrvPnz+vIkSPW4D5hwgTr8omYmBib8fbs2aNHHnlE5cqVU7Vq1fLc1zXr1q1T/fr15e7urqpVq2rWrFk2r+e1rGPbtm2yWCzW5RWtWrXS559/rqNHj9os77gmtyUNP/74o7p27apy5crJ3d1doaGhWrJkSa77WbFihcaMGaNKlSrJ09NTbdu21c8//5z3iQfgMJjhBeCQsrKydOXKFRmGof/973966623dO7cOT322GP52r5fv37q3Lmzli9frmPHjmnkyJF6/PHH9dVXX1n7PPPMM1q4cKEGDx6sBx54QEeOHNErr7yibdu2ac+ePfLx8bH2TU1N1ZNPPqkXX3xRAQEBmj17tp566ikdO3ZMH3/8sUaPHi0vLy9NnDhR3bp102+//aZKlSrlWd+hQ4cUERGhfv36ycvLS0eOHNG0adN0//336z//+U+OYH+rOnXqJGdnZ+3YsSPPPkeOHFHnzp3VvHlzvf/++ypbtqyOHz+uTZs2KTMzU/7+/tq0aZM6dOigvn37ql+/fpKUY/b64YcfVs+ePTVw4ECdO3fuhnUlJSVp2LBhGj9+vCpWrKgPP/xQQ4cOVWZmpl544YUCHeO8efM0YMAAHTp0SOvWrbtp/59//lmRkZHy9fXVrFmz5O3trQ8++EAxMTH63//+pxdffNGm/+jRo9WsWTO9++67ysjI0EsvvaQuXbrowIEDcnZ2LlCtAG4vAi8Ah3T9x89ubm6aM2eO2rdvn6/tO3ToYDNT+Mcff+jFF19UamqqKlasqP/+979auHChnn32Wc2ePdvar2HDhmrSpImmT5+u119/3dp++vRpbd68WWFhYZKk8PBw+fr66s0339Svv/5qDbeVKlVSaGio1qxZo+eeey7P+gYOHGj92TAMRUZGqlWrVgoKCtIXX3yhBx98MF/HmV+lSpWSj49Pjlnuv0tMTNTFixf11ltvqUGDBtb2v/8n49rxBwQE5LlEoE+fPtZ1vjdz4sQJ7d2717q/jh07Ki0tTa+99pqeffZZlSxZMl/jSFLt2rVVtmxZubm55Wv5wvjx45WZmamvv/5agYGBkq7+x+Cvv/7ShAkT9PTTT8vLy8tm/A8++MD63NnZWT169NDu3btZLgE4OJY0AHBIS5cu1e7du7V792598cUX6tOnjwYNGqQ5c+bka/vrA2P9+vUlSUePHpUkff3115Jk/Tj+msaNGyskJERbt261aff397eGPUkqX768fH19FRoaajOTGxISYrOfvKSlpWngwIEKDAyUi4uLSpQooaCgIEnSgQMH8nWMBWUYxg1fDw0NlaurqwYMGKAlS5bot99+K9R+unfvnu++derUsQnX0tWAnZGRoT179hRq//n11VdfqU2bNtawe01MTIzOnz+vhIQEm/abvacAOC5meAE4pJCQkBxfWjt69KhefPFFPf744ypbtuwNt/f29rZ57ubmJkm6cOGCpKsztpJyvXJCpUqVcoSY8uXL5+jn6uqao93V1VWSdPHixTxry87OVlRUlE6cOKFXXnlF9erVU6lSpZSdna2mTZtaayxK586d0+nTp1WvXr08+1SrVk1btmzR5MmTNWjQIJ07d05Vq1bVkCFDNHTo0HzvqyBXo6hYsWKebdd+R8Xl9OnTef7+c9v/zd5TABwXM7wA7hj169fXhQsX9Msvv9zyWNfCS0pKSo7XTpw4YbN+t6j9+OOP2rdvn9566y0999xzatWqle67774cgaooff7558rKyrrppcSaN2+uzz77TOnp6frmm28UERGhYcOGaeXKlfneV0Gu7Zuamppn27Xz4e7uLkm6dOmSTb9b/YKft7d3nr9/ScX6HgBwexF4AdwxkpKSJOX8klRh/OMf/5AkmzWZkrR7924dOHBAbdq0ueV95OVaILw2Q3jN36+gUJSSk5P1wgsvyMvLS08//XS+tnF2dlaTJk00d+5cSbIuLyjqWc2ffvpJ+/bts2lbvny5ypQpo0aNGkm6eoMKSfrhhx9s+q1fvz7HeG5ubvmurU2bNvrqq69yrGteunSpSpYsybpcwERY0gDAIf3444+6cuWKpKsfLa9du1ZxcXF66KGHFBwcfMvj16xZUwMGDNDs2bPl5OSkjh07Wq/SEBgYqOHDh9/yPvJSq1YtVatWTS+//LIMw1D58uX12WefKS4u7pbHvnberly5orS0NO3cuVOLFi2Ss7Oz1q1bd8P/LLz99tv66quv1LlzZ1WuXFkXL17U+++/L0nWG1aUKVNGQUFB+vTTT9WmTRuVL19ePj4+1lBaUJUqVdKDDz6o8ePHy9/fXx988IHi4uI0adIk6xfW7rvvPtWsWVMvvPCCrly5onLlymndunXatWtXjvHq1auntWvXav78+QoLC5OTk5PN0pi/GzdunDZs2KDWrVvr1VdfVfny5fXhhx/q888/1+TJk22+sAbgzkbgBeCQnnzySevPXl5eCg4O1rRp0/Tss88W2T7mz5+vatWq6b333tPcuXPl5eWlDh06KDY2tliXF5QoUUKfffaZhg4dqqefflouLi5q27attmzZosqVK9/S2NfOm6urq8qWLauQkBC99NJL6tev301nxkNDQ/Xll19q3LhxSk1NVenSpVW3bl2tX79eUVFR1n7vvfeeRo4cqQcffFCXLl1Snz59tHjx4kLVGxoaqieffFLjxo3TwYMHValSJU2bNs3mPxzOzs767LPPNHjwYA0cOFBubm7q2bOn5syZo86dO9uMN3ToUP30008aPXq00tPTZRhGnl/Wq1mzpuLj4zV69GgNGjRIFy5cUEhIiBYtWpTjy4wA7mwW42Zf2wUAAADuYKzhBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqXIc3F9nZ2Tpx4oTKlClToFtkAgAA4PYwDENnzpxRpUqV5OR04zlcAm8uTpw4ocDAQHuXAQAAgJs4duyYAgICbtiHwJuLMmXKSLp6Aj09Pe1cDQAAAK6XkZGhwMBAa267EQJvLq4tY/D09CTwAgAAOLD8LD/lS2sAAAAwNQIvAAAATI3ACwAAAFNjDS8AAA7MMAxduXJFWVlZ9i4FuO1KlCghZ2fnWx6HwAsAgIPKzMxUSkqKzp8/b+9SALuwWCwKCAhQ6dKlb2kcAi8AAA4oOztbhw8flrOzsypVqiRXV1duhoS7imEYOnnypH7//XfVqFHjlmZ6CbwAADigzMxMZWdnKzAwUCVLlrR3OYBdVKhQQUeOHNHly5dvKfDypTUAABzYzW6ZCphZUX2qwb8iAAAAmBqBFwAAAKZm98A7b948BQcHy93dXWFhYdq5c2eefXft2qVmzZrJ29tbHh4eqlWrlqZPn56j35o1a1S7dm25ubmpdu3aWrduXXEeAgAAAByYXQPvqlWrNGzYMI0ZM0Z79+5V8+bN1bFjRyUnJ+fav1SpUho8eLB27NihAwcOaOzYsRo7dqwWLlxo7ZOQkKDo6Gj17t1b+/btU+/evdWjRw99++23t+uwAAC4q8XExMhisWjgwIE5Xnv22WdlsVgUExNz+wvDXctiGIZhr503adJEjRo10vz5861tISEh6tatm2JjY/M1xsMPP6xSpUpp2bJlkqTo6GhlZGToiy++sPbp0KGDypUrpxUrVuQ6xqVLl3Tp0iXr84yMDAUGBio9PV2enp6FOTQAAG7JxYsXdfjwYeunoNnZ0unT9qvH21vK7/fnYmJi9NVXXykjI0MpKSny8PCQdPWY/P395enpqdatW2vx4sXFVzBM4fp/B3+XkZEhLy+vfOU1u83wZmZmKjExUVFRUTbtUVFRio+Pz9cYe/fuVXx8vFq2bGltS0hIyDFm+/btbzhmbGysvLy8rI/AwMACHAkAAMXv9GnJ19d+j4KG7UaNGqly5cpau3attW3t2rUKDAxUw4YNrW2GYWjy5MmqWrWqPDw81KBBA3388cfW17OystS3b18FBwfLw8NDNWvW1MyZM232FRMTo27dumnKlCny9/eXt7e3Bg0apMuXLxfuZMN07BZ4T506paysLPn5+dm0+/n5KTU19YbbBgQEyM3NTeHh4Ro0aJD69etnfS01NbXAY44aNUrp6enWx7FjxwpxRAAA4O+efPJJLVq0yPr8/fff11NPPWXTZ+zYsVq0aJHmz5+vn376ScOHD9fjjz+u7du3S7p6A46AgACtXr1a+/fv16uvvqrRo0dr9erVNuN8/fXXOnTokL7++mstWbJEixcvZgYZVna/8cT111czDOOm11zbuXOnzp49q2+++UYvv/yyqlevrkcffbTQY7q5ucnNza0Q1QMAgLz07t1bo0aN0pEjR2SxWPTvf/9bK1eu1LZt2yRJ586d07Rp0/TVV18pIiJCklS1alXt2rVLCxYsUMuWLVWiRAlNmDDBOmZwcLDi4+O1evVq9ejRw9perlw5zZkzR87OzqpVq5Y6d+6srVu3qn///rf1mOGY7BZ4fXx85OzsnGPmNS0tLccM7fWCg4MlSfXq1dP//vc/jR8/3hp4K1asWKgxAQBA0fLx8VHnzp21ZMkSGYahzp07y8fHx/r6/v37dfHiRbVr185mu8zMTJtlD2+//bbeffddHT16VBcuXFBmZqZCQ0NttqlTp47Nnbj8/f31n//8p3gODHccuwVeV1dXhYWFKS4uTg899JC1PS4uTl27ds33OIZh2HzhLCIiQnFxcRo+fLi17csvv1RkZGTRFA4AgB14e0tpafbdf2E89dRTGjx4sCRp7ty5Nq9lZ2dLkj7//HPdc889Nq9d++R19erVGj58uKZOnaqIiAiVKVNGb731Vo6rL5UoUcLmucVisY4P2HVJw4gRI9S7d2+Fh4crIiJCCxcuVHJysvUyJqNGjdLx48e1dOlSSVf/oVSuXFm1atWSdPW6vFOmTNFzzz1nHXPo0KFq0aKFJk2apK5du+rTTz/Vli1btGvXrtt/gAAAFBEnJ6lCBXtXUXAdOnRQZmampKtfIv+7a9fMT05OtvkC+t/t3LlTkZGRevbZZ61thw4dKr6CYUp2DbzR0dE6ffq0Jk6cqJSUFNWtW1cbN25UUFCQJCklJcXmmrzZ2dkaNWqUDh8+LBcXF1WrVk1vvvmmnn76aWufyMhIrVy5UmPHjtUrr7yiatWqadWqVWrSpMltPz7glhTR/cNvif2uWgjAJJydnXXgwAHrz39XpkwZvfDCCxo+fLiys7N1//33KyMjQ/Hx8SpdurT69Omj6tWra+nSpdq8ebOCg4O1bNky7d6927q8EcgPu39p7dlnn7X5X9vfXf/tyueee85mNjcvjzzyiB555JGiKA8AANyiG10j9bXXXpOvr69iY2P122+/qWzZsmrUqJFGjx4tSRo4cKCSkpIUHR0ti8WiRx99VM8++6zN9faBm7HrjSccVUEuZAwUG2Z4gbvajS64D9wt7vgbTwAAAAC3A4EXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAACY2pEjR2SxWJSUlHRHjV0YixcvVtmyZR1mHEdB4AUAAEUqLS1NTz/9tCpXriw3NzdVrFhR7du3V0JCgrWPxWLRJ598Yr8ib6NWrVrJYrHIYrHIzc1N99xzj7p06aK1a9cW+b6io6P1yy+/FGibKlWqaMaMGbc8jiMj8AIAgCLVvXt37du3T0uWLNEvv/yi9evXq1WrVvrjjz/sXVqhZWZm3tL2/fv3V0pKin799VetWbNGtWvXVs+ePTVgwIAiqvAqDw8P+fr6Osw4joLACwAAisxff/2lXbt2adKkSWrdurWCgoLUuHFjjRo1Sp07d5Z0dUZRkh566CFZLBbr80OHDqlr167y8/NT6dKldd9992nLli0241epUkVvvPGGnnrqKZUpU0aVK1fWwoULbfp89913atiwodzd3RUeHq69e/favJ6VlaW+ffsqODhYHh4eqlmzpmbOnGnTJyYmRt26dVNsbKwqVaqke++9N19j56VkyZKqWLGiAgMD1bRpU02aNEkLFizQO++8Y3OMx48fV3R0tMqVKydvb2917dpVR44ckSRt3rxZ7u7u+uuvv2zGHjJkiFq2bCkp51KEm53TVq1a6ejRoxo+fLh1Fjq3cSRp/vz5qlatmlxdXVWzZk0tW7bM5nWLxaJ3331XDz30kEqWLKkaNWpo/fr1+To/xc3F3gUAAICCmZYwTdMSpt2WfY2IGKERESPy3b906dIqXbq0PvnkEzVt2lRubm45+uzevVu+vr5atGiROnToIGdnZ0nS2bNn1alTJ/3rX/+Su7u7lixZoi5duujnn39W5cqVrdtPnTpVr732mkaPHq2PP/5YzzzzjFq0aKFatWrp3LlzeuCBB/SPf/xDH3zwgQ4fPqyhQ4fa7D87O1sBAQFavXq1fHx8FB8frwEDBsjf3189evSw9tu6das8PT0VFxcnwzDyNXZB9OnTR88//7zWrl2rtm3b6vz582rdurWaN2+uHTt2yMXFRf/617/UoUMH/fDDD2rbtq3Kli2rNWvWqG/fvpKuhvfVq1dr4sSJue7jZud07dq1atCggQYMGKD+/fvnWeu6des0dOhQzZgxQ23bttWGDRv05JNPKiAgQK1bt7b2mzBhgiZPnqy33npLs2fPVq9evXT06FGVL1++0OepKBB4AQC4w2RcytDxM8dv274KwsXFRYsXL1b//v319ttvq1GjRmrZsqV69uyp+vXrS5IqVKggSSpbtqwqVqxo3bZBgwZq0KCB9fm//vUvrVu3TuvXr9fgwYOt7Z06ddKzzz4rSXrppZc0ffp0bdu2TbVq1dKHH36orKwsvf/++ypZsqTq1Kmj33//Xc8884x1+xIlSmjChAnW58HBwYqPj9fq1attAm+pUqX07rvvytXVVZK0cOHCm45dEE5OTrr33nutM7grV66Uk5OT3n33XetM66JFi1S2bFlt27ZNUVFRio6O1vLly62Bd+vWrfrzzz/1z3/+M9d93Oycli9fXs7OzipTpozN7+J6U6ZMUUxMjPW8jxgxQt98842mTJliE3hjYmL06KOPSpLeeOMNzZ49W9999506dOhQqHNUVAi8AADcYTzdPHVPmXtu274Kqnv37urcubN27typhIQEbdq0SZMnT9a7776rmJiYPLc7d+6cJkyYoA0bNujEiRO6cuWKLly4oOTkZJt+14KzdPVj9IoVKyotLU2SdODAATVo0EAlS5a09omIiMixr7ffflvvvvuujh49qgsXLigzM1OhoaE2ferVq2cNuwUZuyAMw7CG28TERP36668qU6aMTZ+LFy/q0KFDkqRevXopIiJCJ06cUKVKlfThhx+qU6dOKleuXK7j5/ec3syBAwdyrDdu1qxZjqUgf//dlCpVSmXKlLH+buyJwAsAwB2moMsM7MHd3V3t2rVTu3bt9Oqrr6pfv34aN27cDQPvyJEjtXnzZk2ZMkXVq1eXh4eHHnnkkRxfGCtRooTNc4vFouzsbElXA+TNrF69WsOHD9fUqVMVERGhMmXK6K233tK3335r069UqVI2z/MzdkFkZWXp4MGDuu+++yRdXWoRFhamDz/8MEffa7PijRs3VrVq1bRy5Uo988wzWrdunRYtWpTnPvJ7TvPjWjC/5u9h/Zob/W7sicALAACKXe3atW0uQ1aiRAllZWXZ9Nm5c6diYmL00EMPSbq6/vTax/0F2c+yZct04cIFeXh4SJK++eabHPuJjIy0fjwvyTqDeqtjF8SSJUv0559/qnv37pKkRo0aadWqVfL19ZWnZ94z64899pg+/PBDBQQEyMnJyfplwNzk55y6urrm+F1cLyQkRLt27dITTzxhbYuPj1dISMjNDtMhcJUGAABQZE6fPm39UtcPP/ygw4cP66OPPtLkyZPVtWtXa78qVapo69atSk1N1Z9//ilJql69utauXaukpCTt27dPjz32WIFnBx977DE5OTmpb9++2r9/vzZu3KgpU6bY9Klevbq+//57bd68Wb/88oteeeUV7d69u0jGzsv58+eVmpqq33//Xd9++61eeuklDRw4UM8884x1DWyvXr3k4+Ojrl27aufOnTp8+LC2b9+uoUOH6vfff7eO1atXL+3Zs0evv/66HnnkEbm7u+e53/yc0ypVqmjHjh06fvy4Tp06les4I0eO1OLFi/X222/r4MGDmjZtmtauXasXXnghX8dvbwReAABQZEqXLq0mTZpo+vTpatGiherWratXXnlF/fv315w5c6z9pk6dqri4OAUGBqphw4aSpOnTp6tcuXKKjIxUly5d1L59ezVq1KjA+//ss8+0f/9+NWzYUGPGjNGkSZNs+gwcOFAPP/ywoqOj1aRJE50+fdpmtvdWxs7LO++8I39/f1WrVk0PPfSQ9u/fr1WrVmnevHnWPiVLltSOHTtUuXJlPfzwwwoJCdFTTz2lCxcu2Mz41qhRQ/fdd59++OEH9erV64b7zc85nThxoo4cOaJq1apZl05cr1u3bpo5c6beeust1alTRwsWLNCiRYvUqlWrfB2/vVmMol6QYgIZGRny8vJSenr6DT9SAIrVdeui7II/D4DdXLx4UYcPH1ZwcPANZ/AAM7vRv4OC5DVmeAEAAGBqBF4AAACYGldpAFAo2dnS6dO2bd7ekhP/jQYAOBgCL4BCOX1a8vW1bUtLk/L4vgMAAHbDXAwAAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAMCdxmK5fY9icuTIEVksFiUlJRXbPvJr/PjxCg0NLdA2FotFn3zySYG2adWqlYYNG2Z9XqVKFc2YMaNAY+RHTEyMunXrlud+i3NfjorACwAAilRMTIwsFov14e3trQ4dOuiHH36w9gkMDFRKSorq1q1rx0qveuGFF7R169bbvt/du3drwIAB+epbkHA8c+ZMLV68uPCF5SKv/6AUx76KA4EXAAAUuQ4dOiglJUUpKSnaunWrXFxc9MADD1hfd3Z2VsWKFeXiYv9bApQuXVre3t63fb8VKlRQyZIli2y8rKwsZWdny8vLS2XLli2ycW/kdu7rVhB4AQBAkXNzc1PFihVVsWJFhYaG6qWXXtKxY8d08uRJSTlnDLdt2yaLxaKtW7cqPDxcJUuWVGRkpH7++WebcefPn69q1arJ1dVVNWvW1LJly2xet1gsWrBggR544AGVLFlSISEhSkhI0K+//qpWrVqpVKlSioiI0KFDh6zbXL+kYffu3WrXrp18fHzk5eWlli1bas+ePQU6/nPnzumJJ55Q6dKl5e/vr6lTp+boc/2s7fjx41W5cmW5ubmpUqVKGjJkiKSrSxKOHj2q4cOHW2fNJWnx4sUqW7asNmzYoNq1a8vNzU1Hjx7NdZnBlStXNHjwYJUtW1be3t4aO3asDMOwOW/XL9EoW7asdfY2ODhYktSwYUNZLBa1atVKUs4lDZcuXdKQIUPk6+srd3d33X///dq9e7f19fz+nosagRcAABSrs2fP6sMPP1T16tVvOpM6ZswYTZ06Vd9//71cXFz01FNPWV9bt26dhg4dqueff14//vijnn76aT355JP6+uuvbcZ47bXX9MQTTygpKUm1atXSY489pqefflqjRo3S999/L0kaPHhwnjWcOXNGffr00c6dO/XNN9+oRo0a6tSpk86cOZPvYx45cqS+/vprrVu3Tl9++aW2bdumxMTEPPt//PHHmj59uhYsWKCDBw/qk08+Ub169SRJa9euVUBAgCZOnGidNb/m/Pnzio2N1bvvvquffvpJvtffAvP/W7JkiVxcXPTtt99q1qxZmj59ut599918H893330nSdqyZYtSUlK0du3aXPu9+OKLWrNmjZYsWaI9e/aoevXqat++vf744w+bfjf6PRcLAzmkp6cbkoz09HR7l4K7mWT/xw2kpeXsnpZ2m84NcBe4cOGCsX//fuPChQs5X3SQvwN56dOnj+Hs7GyUKlXKKFWqlCHJ8Pf3NxITE619Dh8+bEgy9u7daxiGYXz99deGJGPLli3WPp9//rkhyXoOIiMjjf79+9vs65///KfRqVOnv50aGWPHjrU+T0hIMCQZ7733nrVtxYoVhru7u/X5uHHjjAYNGuR5PFeuXDHKlCljfPbZZzb7WbduXa79z5w5Y7i6uhorV660tp0+fdrw8PAwhg4dam0LCgoypk+fbhiGYUydOtW49957jczMzFzH/HvfaxYtWmRIMpKSkmza+/TpY3Tt2tX6vGXLlkZISIiRnZ1tbXvppZeMkJCQGx6Pl5eXsWjRIsMwcv6+ctvX2bNnjRIlShgffvih9fXMzEyjUqVKxuTJkw3DyN/v+e9u9O+gIHmNGV4AAFDkWrduraSkJCUlJenbb79VVFSUOnbsqKNHj95wu/r161t/9vf3lySlpaVJkg4cOKBmzZrZ9G/WrJkOHDiQ5xh+fn6SZJ0tvdZ28eJFZWRk5FpDWlqaBg4cqHvvvVdeXl7y8vLS2bNnlZycfLPDliQdOnRImZmZioiIsLaVL19eNWvWzHObf/7zn7pw4YKqVq2q/v37a926dbpy5cpN9+Xq6mpzvHlp2rSpdSmEJEVEROjgwYPKysq66bb5dejQIV2+fNnmd1SiRAk1btz4hr+j63/PxYHACwAAilypUqVUvXp1Va9eXY0bN9Z7772nc+fO6Z133rnhdiVKlLD+fC2gZWdn52i7xjCMHG25jXGzcf8uJiZGiYmJmjFjhuLj45WUlCRvb29lZmbesPa/11RQgYGB+vnnnzV37lx5eHjo2WefVYsWLXT58uUbbufh4ZHj+AvDYrHkqPtm+77ete0L+zvK6/dRFAi8AACg2FksFjk5OenChQuFHiMkJES7du2yaYuPj1dISMitlmdj586dGjJkiDp16qQ6derIzc1Np06dyvf21atXV4kSJfTNN99Y2/7880/98ssvN9zOw8NDDz74oGbNmqVt27YpISFB//nPfyRdncm9ldnYv9dy7XmNGjXk7Ows6eoVI/6+NvjgwYM6f/689bmrq6sk3bCG6tWry9XV1eZ3dPnyZX3//fdF/jsqKPtfCwQAAJjOpUuXlJqaKulq2JszZ47Onj2rLl26FHrMkSNHqkePHmrUqJHatGmjzz77TGvXrtWWLVuKqmxJV4PbsmXLFB4eroyMDI0cOVIeHh753r506dLq27evRo4cKW9vb/n5+WnMmDFycsp7nnHx4sXKyspSkyZNVLJkSS1btkweHh4KCgqSdPWKDjt27FDPnj3l5uYmHx+fAh3TsWPHNGLECD399NPas2ePZs+ebXPliH/84x+aM2eOmjZtquzsbL300ks2s7C+vr7y8PDQpk2bFBAQIHd3d3l5ednso1SpUnrmmWc0cuRIlS9fXpUrV9bkyZN1/vx59e3bt0D1FjVmeAEAuNPczq+tFdKmTZvk7+8vf39/NWnSRLt379ZHH31kvZxVYXTr1k0zZ87UW2+9pTp16mjBggVatGjRLY2Zm/fff19//vmnGjZsqN69e1svs1UQb731llq0aKEHH3xQbdu21f3336+wsLA8+5ctW1bvvPOOmjVrpvr162vr1q367LPPrFe1mDhxoo4cOaJq1aqpQoUKBT6mJ554QhcuXFDjxo01aNAgPffcczY3vZg6daoCAwPVokULPfbYY3rhhRdsrhHs4uKiWbNmacGCBapUqZK6du2a637efPNNde/eXb1791ajRo3066+/avPmzSpXrlyBay5KFqMwC01MLiMjQ15eXkpPT5enp6e9y8Hdqhhv6ZlvN/jzcPKkdP3f/7Q0qRB/hwHk4uLFizp8+LCCg4Pl7u5u73IAu7jRv4OC5DVmeAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAcGB8txx3s6J6/xN4AQBwQNeugfr3i/8Dd5trd7e7doOMwuLGEwAAOCBnZ2eVLVtWaWlpkqSSJUsWyS1kgTtFdna2Tp48qZIlS8rF5dYiK4EXAAAHVbFiRUmyhl7gbuPk5KTKlSvf8n/2CLwAADgoi8Uif39/+fr66vLly/YuB7jtXF1db3hL5vwi8AIA4OCcnZ1veQ0jcDfjS2sAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUuPEEAMd2i7eTLDKGYe8KAACFxAwvAAAATM3ugXfevHkKDg6Wu7u7wsLCtHPnzjz7rl27Vu3atVOFChXk6empiIgIbd682abP4sWLZbFYcjwuXrxY3IcCAAAAB2TXwLtq1SoNGzZMY8aM0d69e9W8eXN17NhRycnJufbfsWOH2rVrp40bNyoxMVGtW7dWly5dtHfvXpt+np6eSklJsXm4u7vfjkMCAACAg7EYhv0WpjVp0kSNGjXS/PnzrW0hISHq1q2bYmNj8zVGnTp1FB0drVdffVXS1RneYcOG6a+//ip0XRkZGfLy8lJ6ero8PT0LPQ5wSxxh7eoN/jycPCn5+tq2paVJFSoUcQ2OcB4k1vACgIMpSF6z2wxvZmamEhMTFRUVZdMeFRWl+Pj4fI2RnZ2tM2fOqHz58jbtZ8+eVVBQkAICAvTAAw/kmAG+3qVLl5SRkWHzAAAAgDnYLfCeOnVKWVlZ8vPzs2n38/NTampqvsaYOnWqzp07px49eljbatWqpcWLF2v9+vVasWKF3N3d1axZMx08eDDPcWJjY+Xl5WV9BAYGFu6gAAAA4HDs/qU1y3UfVxqGkaMtNytWrND48eO1atUq+f7tc9WmTZvq8ccfV4MGDdS8eXOtXr1a9957r2bPnp3nWKNGjVJ6err1cezYscIfEAAAAByK3a7D6+PjI2dn5xyzuWlpaTlmfa+3atUq9e3bVx999JHatm17w75OTk667777bjjD6+bmJjc3t/wXDwAAgDuG3WZ4XV1dFRYWpri4OJv2uLg4RUZG5rndihUrFBMTo+XLl6tz58433Y9hGEpKSpK/v/8t1wwAAIA7j13vtDZixAj17t1b4eHhioiI0MKFC5WcnKyBAwdKurrU4Pjx41q6dKmkq2H3iSee0MyZM9W0aVPr7LCHh4e8vLwkSRMmTFDTpk1Vo0YNZWRkaNasWUpKStLcuXPtc5AAAACwK7sG3ujoaJ0+fVoTJ05USkqK6tatq40bNyooKEiSlJKSYnNN3gULFujKlSsaNGiQBg0aZG3v06ePFi9eLEn666+/NGDAAKWmpsrLy0sNGzbUjh071Lhx49t6bAAAAHAMdr0Or6PiOrxwCI5w/Vmuw/t/+FMJAA7ljrgOLwAAAHA7EHgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmJrdA++8efMUHBwsd3d3hYWFaefOnXn2Xbt2rdq1a6cKFSrI09NTERER2rx5c45+a9asUe3ateXm5qbatWtr3bp1xXkIAAAAcGB2DbyrVq3SsGHDNGbMGO3du1fNmzdXx44dlZycnGv/HTt2qF27dtq4caMSExPVunVrdenSRXv37rX2SUhIUHR0tHr37q19+/apd+/e6tGjh7799tvbdVgAAABwIBbDMAx77bxJkyZq1KiR5s+fb20LCQlRt27dFBsbm68x6tSpo+joaL366quSpOjoaGVkZOiLL76w9unQoYPKlSunFStW5GvMjIwMeXl5KT09XZ6engU4IqAIWSz2rkC6wZ+HkyclX1/btrQ0qUKFIq7BEc6DdMNzAQC4/QqS1+w2w5uZmanExERFRUXZtEdFRSk+Pj5fY2RnZ+vMmTMqX768tS0hISHHmO3bt7/hmJcuXVJGRobNAwAAAOZgt8B76tQpZWVlyc/Pz6bdz89Pqamp+Rpj6tSpOnfunHr06GFtS01NLfCYsbGx8vLysj4CAwMLcCQAAABwZHb/0prluo8rDcPI0ZabFStWaPz48Vq1apV8r/tctaBjjho1Sunp6dbHsWPHCnAEAAAAcGQu9tqxj4+PnJ2dc8y8pqWl5Zihvd6qVavUt29fffTRR2rbtq3NaxUrVizwmG5ubnJzcyvgEQAAAOBOYLcZXldXV4WFhSkuLs6mPS4uTpGRkXlut2LFCsXExGj58uXq3LlzjtcjIiJyjPnll1/ecEw4GIvF/g8AAGAadpvhlaQRI0aod+/eCg8PV0REhBYuXKjk5GQNHDhQ0tWlBsePH9fSpUslXQ27TzzxhGbOnKmmTZtaZ3I9PDzk5eUlSRo6dKhatGihSZMmqWvXrvr000+1ZcsW7dq1yz4HCQAAALuy6xre6OhozZgxQxMnTlRoaKh27NihjRs3KigoSJKUkpJic03eBQsW6MqVKxo0aJD8/f2tj6FDh1r7REZGauXKlVq0aJHq16+vxYsXa9WqVWrSpMltPz4AAADYn12vw+uouA6vnTnCkgJH+Gfh4OeB6/ACAOzpjrgOLwAAAHA7EHgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpEXgBAABgagReAAAAmBqBFwAAAKZG4AUAAICpFSrwLl68WOfPny/qWgAAAIAiV6jAO2rUKFWsWFF9+/ZVfHx8UdcEAAAAFJlCBd7ff/9dH3zwgf7880+1bt1atWrV0qRJk5SamlrU9QEAAAC3pFCB19nZWQ8++KDWrl2rY8eOacCAAfrwww9VuXJlPfjgg/r000+VnZ1d1LUCAAAABXbLX1rz9fVVs2bNFBERIScnJ/3nP/9RTEyMqlWrpm3bthVBiQAAAEDhFTrw/u9//9OUKVNUp04dtWrVShkZGdqwYYMOHz6sEydO6OGHH1afPn2KslYAAACgwCyGYRgF3ahLly7avHmz7r33XvXr109PPPGEypcvb9PnxIkTCggIuCOXNmRkZMjLy0vp6eny9PS0dzl3H4vF3hVIBf9nUfQc/DycPCn5+tq2paVJFSoUcQ2OcB4kx3hPAACsCpLXXAqzA19fX23fvl0RERF59vH399fhw4cLMzwAAABQZAq1pKFly5Zq1KhRjvbMzEwtXbpUkmSxWBQUFHRr1QEAAAC3qFCB98knn1R6enqO9jNnzujJJ5+85aIAAACAolKowGsYhiy5rKv7/fff5eXldctFAQAAAEWlQGt4GzZsKIvFIovFojZt2sjF5f82z8rK0uHDh9WhQ4ciLxIAAAAorAIF3m7dukmSkpKS1L59e5UuXdr6mqurq6pUqaLu3bsXaYEAAADArShQ4B03bpwkqUqVKoqOjpa7u3uxFAUAAAAUlUJdlowbSgAAAOBOke/AW758ef3yyy/y8fFRuXLlcv3S2jV//PFHkRQHAAAA3Kp8B97p06erTJky1p9vFHgBAAAAR1GoWwubHbcWtjNH+M+UI/yzcPDzwK2FAQD2VCy3Fs7IyMh3AYREAAAAOIp8B96yZcvedBnDtRtSZGVl3XJhAAAAQFHId+D9+uuvi7MOAAAAoFjkO/C2bNmyOOsAAAAAikW+A+8PP/ygunXrysnJST/88MMN+9avX/+WCwMAAACKQr4Db2hoqFJTU+Xr66vQ0FBZLBbldoEH1vACAADAkeQ78B4+fFgV/v/1hg4fPlxsBQEAAABFKd+BNygoKNefAQAAAEeW78B7vZ9//lmzZ8/WgQMHZLFYVKtWLT333HOqWbNmUdYHAAAA3BKnwmz08ccfq27dukpMTFSDBg1Uv3597dmzR3Xr1tVHH31U1DUCAAAAhVaoWwtXrVpVjz/+uCZOnGjTPm7cOC1btky//fZbkRVoD9xa2M4c4VayjnAbWQc/D9xaGABgTwXJa4Wa4U1NTdUTTzyRo/3xxx9XampqYYYEAAAAikWhAm+rVq20c+fOHO27du1S8+bNb7koAAAAoKjk+0tr69evt/784IMP6qWXXlJiYqKaNm0qSfrmm2/00UcfacKECUVfJQAAAFBI+V7D6+SUv8lgM9x4gjW8duYIazYdYb2mg58H1vACAOypIHkt3zO82dnZt1wYAAAAcLsVag0vAAAAcKco9I0nzp07p+3btys5OVmZmZk2rw0ZMuSWCwMAAACKQqEC7969e9WpUyedP39e586dU/ny5XXq1CmVLFlSvr6+BF4AAAA4jEItaRg+fLi6dOmiP/74Qx4eHvrmm2909OhRhYWFacqUKUVdIwAAAFBohQq8SUlJev755+Xs7CxnZ2ddunRJgYGBmjx5skaPHl2gsebNm6fg4GC5u7srLCws1+v7XpOSkqLHHntMNWvWlJOTk4YNG5ajz+LFi2WxWHI8Ll68WNDDBAAAgAkUKvCWKFFClv9/qSA/Pz8lJydLkry8vKw/58eqVas0bNgwjRkzRnv37lXz5s3VsWPHPMe4dOmSKlSooDFjxqhBgwZ5juvp6amUlBSbh7u7ewGOEAAAAGZRqDW8DRs21Pfff697771XrVu31quvvqpTp05p2bJlqlevXr7HmTZtmvr27at+/fpJkmbMmKHNmzdr/vz5io2NzdG/SpUqmjlzpiTp/fffz3Nci8WiihUrFvCoAAAAYEaFmuF944035O/vL0l67bXX5O3trWeeeUZpaWlauHBhvsbIzMxUYmKioqKibNqjoqIUHx9fmLKszp49q6CgIAUEBOiBBx7Q3r17b9j/0qVLysjIsHkAAADAHAo1wxseHm79uUKFCtq4cWOBxzh16pSysrLk5+dn0+7n56fU1NTClCVJqlWrlhYvXqx69eopIyNDM2fOVLNmzbRv3z7VqFEj121iY2O5JTIAAIBJFfo6vJKUlpamn3/+WRaLRTVr1lSFQtxT1HLdbUMNw8jRVhBNmzZV06ZNrc+bNWumRo0aafbs2Zo1a1au24waNUojRoywPs/IyFBgYGChawAAAIDjKFTgzcjI0KBBg7Ry5UplZWVJkpydnRUdHa25c+fKy8vrpmP4+PjI2dk5x2xuWlpajlnfW+Hk5KT77rtPBw8ezLOPm5ub3NzcimyfKJxpCdM0LWGaNOLmfYvdtAB7V+Dw5yE7WzlqbLBEcirq+zc6wnmQHOM9UUxGRIzQiAhHOdEAUPQKFXj79eunpKQkbdiwQREREbJYLIqPj9fQoUPVv39/rV69+qZjuLq6KiwsTHFxcXrooYes7XFxceratWthysqVYRhKSkoq0JfpYB8ZlzJ0/MxxydPelUg6c9zeFdwZ5+G6GlPOFUMNjnAeJMd4TxSTjEt8bwGAuRUq8H7++efavHmz7r//fmtb+/bt9c4776hDhw75HmfEiBHq3bu3wsPDFRERoYULFyo5OVkDBw6UdHWpwfHjx7V06VLrNklJSZKufjHt5MmTSkpKkqurq2rXri1JmjBhgpo2baoaNWooIyNDs2bNUlJSkubOnVuYQ8Vt5OnmqXvK3CMdd4Bgcc899q7A4c9DdraUkmLb5u9fDDO8jnAeJMd4TxQTTzdH+V8FABSPQgVeb2/vXJcteHl5qVy5cvkeJzo6WqdPn9bEiROVkpKiunXrauPGjQoKCpJ09UYT11+Tt2HDhtafExMTtXz5cgUFBenIkSOSpL/++ksDBgxQamqqvLy81LBhQ+3YsUONGzcuxJHidrJ+rHoLa7iLjPG7vStw+PNw8qTk62vbti9NKsRS/htzhPMgOcZ7AgBQKBbDMIyCbrRw4UJ99NFHWrp0qfXyZKmpqerTp48efvhhPf3000Ve6O2UkZEhLy8vpaeny9OTmY/bzhECTsH/WRQ9Bz8PuQXeNFMHXgd4TwAArAqS1/I9w9uwYUObqyccPHhQQUFBqly5siQpOTlZbm5uOnny5B0feAEAAGAe+Q683bp1K8YyAAAAgOKR78A7bty44qwDAAAAKBa3dOOJxMREHThwQBaLRbVr17b5QhkAAADgCAoVeNPS0tSzZ09t27ZNZcuWlWEYSk9PV+vWrbVy5cpC3XENAAAAKA6FumLmc889p4yMDP3000/6448/9Oeff+rHH39URkaGhgwZUtQ1AgAAAIVWqBneTZs2acuWLQoJCbG21a5dW3PnzlVUVFSRFQcAAADcqkLN8GZnZ6tEiRI52kuUKKHs7OxbLgoAAAAoKoUKvP/4xz80dOhQnThxwtp2/PhxDR8+XG3atCmy4gAAAIBbVajAO2fOHJ05c0ZVqlRRtWrVVL16dQUHB+vMmTOaPXt2UdcIAAAAFFqh1vAGBgZqz549iouL03//+18ZhqHatWurbdu2RV0fAAAAcEsKHHivXLkid3d3JSUlqV27dmrXrl1x1AUAAAAUiQIvaXBxcVFQUJCysrKKox4AAACgSBVqDe/YsWM1atQo/fHHH0VdDwAAAFCkCrWGd9asWfr1119VqVIlBQUFqVSpUjav79mzp0iKAwD8jcVi7wokw7B3BQBQYIUKvN26dZPFYpHBHz4AAAA4uAIF3vPnz2vkyJH65JNPdPnyZbVp00azZ8+Wj49PcdUHAAAA3JICreEdN26cFi9erM6dO+vRRx/Vli1b9MwzzxRXbQAAAMAtK9AM79q1a/Xee++pZ8+ekqRevXqpWbNmysrKkrOzc7EUCAAAANyKAs3wHjt2TM2bN7c+b9y4sVxcXGxuMQwAAAA4kgIF3qysLLm6utq0ubi46MqVK0VaFAAAAFBUCrSkwTAMxcTEyM3Nzdp28eJFDRw40ObSZGvXri26CgEAAIBbUKDA26dPnxxtjz/+eJEVAwAAABS1AgXeRYsWFVcdAAAAQLEo1K2FAQAAgDsFgRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqRF4AQAAYGoEXgAAAJgagRcAAACmRuAFAACAqdk98M6bN0/BwcFyd3dXWFiYdu7cmWfflJQUPfbYY6pZs6acnJw0bNiwXPutWbNGtWvXlpubm2rXrq1169YVU/UAAABwdHYNvKtWrdKwYcM0ZswY7d27V82bN1fHjh2VnJyca/9Lly6pQoUKGjNmjBo0aJBrn4SEBEVHR6t3797at2+fevfurR49eujbb78tzkMBAACAg7IYhmHYa+dNmjRRo0aNNH/+fGtbSEiIunXrptjY2Btu26pVK4WGhmrGjBk27dHR0crIyNAXX3xhbevQoYPKlSunFStW5KuujIwMeXl5KT09XZ6envk/IBQNi8XeFUj2+2fxfxz8PJw8Kfn62ralpUkVKhRxDY5wHiTeE9c4wnkAABUsr9lthjczM1OJiYmKioqyaY+KilJ8fHyhx01ISMgxZvv27W845qVLl5SRkWHzAAAAgDnYLfCeOnVKWVlZ8vPzs2n38/NTampqocdNTU0t8JixsbHy8vKyPgIDAwu9fwAAADgWu39pzXLdR3SGYeRoK+4xR40apfT0dOvj2LFjt7R/AAAAOA4Xe+3Yx8dHzs7OOWZe09LScszQFkTFihULPKabm5vc3NwKvU8AAAA4LrvN8Lq6uiosLExxcXE27XFxcYqMjCz0uBERETnG/PLLL29pTAAAANy57DbDK0kjRoxQ7969FR4eroiICC1cuFDJyckaOHCgpKtLDY4fP66lS5dat0lKSpIknT17VidPnlRSUpJcXV1Vu3ZtSdLQoUPVokULTZo0SV27dtWnn36qLVu2aNeuXbf9+AAAAGB/dg280dHROn36tCZOnKiUlBTVrVtXGzduVFBQkKSrN5q4/pq8DRs2tP6cmJio5cuXKygoSEeOHJEkRUZGauXKlRo7dqxeeeUVVatWTatWrVKTJk1u23EBAADAcdj1OryOiuvw2hnXGr3Kwc8D1+G1A0c4F45wHgBAd8h1eAEAAIDbgcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1F3sXgP/PYrF3BVcZhr0rAAAAKFLM8AIAAMDUCLwAAAAwNQIvAAAATI3ACwAAAFMj8AIAAMDUCLwAAAAwNQIvAAAATM3ugXfevHkKDg6Wu7u7wsLCtHPnzhv23759u8LCwuTu7q6qVavq7bfftnl98eLFslgsOR4XL14szsMAAACAg7Jr4F21apWGDRumMWPGaO/evWrevLk6duyo5OTkXPsfPnxYnTp1UvPmzbV3716NHj1aQ4YM0Zo1a2z6eXp6KiUlxebh7u5+Ow4JAAAADsaud1qbNm2a+vbtq379+kmSZsyYoc2bN2v+/PmKjY3N0f/tt99W5cqVNWPGDElSSEiIvv/+e02ZMkXdu3e39rNYLKpYseJtOQYAAAA4NrvN8GZmZioxMVFRUVE27VFRUYqPj891m4SEhBz927dvr++//16XL1+2tp09e1ZBQUEKCAjQAw88oL17996wlkuXLikjI8PmAQAAAHOwW+A9deqUsrKy5OfnZ9Pu5+en1NTUXLdJTU3Ntf+VK1d06tQpSVKtWrW0ePFirV+/XitWrJC7u7uaNWumgwcP5llLbGysvLy8rI/AwMBbPDoAAAA4Crt/ac1isdg8NwwjR9vN+v+9vWnTpnr88cfVoEEDNW/eXKtXr9a9996r2bNn5znmqFGjlJ6ebn0cO3assIcDAAAAB2O3Nbw+Pj5ydnbOMZublpaWYxb3mooVK+ba38XFRd7e3rlu4+TkpPvuu++GM7xubm5yc3Mr4BEAAADgTmC3GV5XV1eFhYUpLi7Opj0uLk6RkZG5bhMREZGj/5dffqnw8HCVKFEi120Mw1BSUpL8/f2LpnAAAADcUex6lYYRI0aod+/eCg8PV0REhBYuXKjk5GQNHDhQ0tWlBsePH9fSpUslSQMHDtScOXM0YsQI9e/fXwkJCXrvvfe0YsUK65gTJkxQ06ZNVaNGDWVkZGjWrFlKSkrS3Llz7XKMAIAidoNlb7fN/19OB+DOYNfAGx0drdOnT2vixIlKSUlR3bp1tXHjRgUFBUmSUlJSbK7JGxwcrI0bN2r48OGaO3euKlWqpFmzZtlckuyvv/7SgAEDlJqaKi8vLzVs2FA7duxQ48aNb/vxAQAAwP4shsF/U6+XkZEhLy8vpaeny9PT8/bs1BFmLCTHmLVwhHPBebjqBufh5EnJ19e2LS1NqlChiGtwhPMg8Z64hvNwlSOcB+AuV5C8ZverNAAAAADFicALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1Ai8AAABMjcALAAAAUyPwAgAAwNQIvAAAADA1F3sXAAAACsFisXcFkmHYuwIgX5jhBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqdg+88+bNU3BwsNzd3RUWFqadO3fesP/27dsVFhYmd3d3Va1aVW+//XaOPmvWrFHt2rXl5uam2rVra926dcVVPgAAABycXQPvqlWrNGzYMI0ZM0Z79+5V8+bN1bFjRyUnJ+fa//Dhw+rUqZOaN2+uvXv3avTo0RoyZIjWrFlj7ZOQkKDo6Gj17t1b+/btU+/evdWjRw99++23t+uwAAAA4EAshmEY9tp5kyZN1KhRI82fP9/aFhISom7duik2NjZH/5deeknr16/XgQMHrG0DBw7Uvn37lJCQIEmKjo5WRkaGvvjiC2ufDh06qFy5clqxYkW+6srIyJCXl5fS09Pl6elZ2MMrGIvl9uznZuz3dvg/jnAuOA9X3eA8nDwp+fratqWlSRUqFHENjnAeJN4T13AeruI8XOUI50HiXNylCpLXXG5TTTlkZmYqMTFRL7/8sk17VFSU4uPjc90mISFBUVFRNm3t27fXe++9p8uXL6tEiRJKSEjQ8OHDc/SZMWNGnrVcunRJly5dsj5PT0+XdPVE3nXuxmPODefhqhuchzNncm9zcyvGeuyJ98RVnIerOA9XcR7+D+fitruW0/Izd2u3wHvq1CllZWXJz8/Ppt3Pz0+pqam5bpOamppr/ytXrujUqVPy9/fPs09eY0pSbGysJkyYkKM9MDAwv4djHl5e9q7AMXAerirgeahWrZjqcAS8J67iPFzFebiK8/B/OBd2c+bMGXnd5PzbLfBeY7nuYwjDMHK03az/9e0FHXPUqFEaMWKE9Xl2drb++OMPeXt733A7R5KRkaHAwEAdO3bs9i3DgEPjPYG/4/2A6/GewPXutPeEYRg6c+aMKlWqdNO+dgu8Pj4+cnZ2zjHzmpaWlmOG9pqKFSvm2t/FxUXe3t437JPXmJLk5uYmt+s+hy1btmx+D8WheHp63hFvUtw+vCfwd7wfcD3eE7jenfSeuNnM7jV2u0qDq6urwsLCFBcXZ9MeFxenyMjIXLeJiIjI0f/LL79UeHi4SpQoccM+eY0JAAAAc7PrkoYRI0aod+/eCg8PV0REhBYuXKjk5GQNHDhQ0tWlBsePH9fSpUslXb0iw5w5czRixAj1799fCQkJeu+992yuvjB06FC1aNFCkyZNUteuXfXpp59qy5Yt2rVrl12OEQAAAPZl18AbHR2t06dPa+LEiUpJSVHdunW1ceNGBQUFSZJSUlJsrskbHBysjRs3avjw4Zo7d64qVaqkWbNmqXv37tY+kZGRWrlypcaOHatXXnlF1apV06pVq9SkSZPbfny3k5ubm8aNG5djaQbuXrwn8He8H3A93hO4npnfE3a9Di8AAABQ3Ox+a2EAAACgOBF4AQAAYGoEXgAAAJgagRcAAACmRuA1gXnz5ik4OFju7u4KCwvTzp077V0S7CQ2Nlb33XefypQpI19fX3Xr1k0///yzvcuCA4mNjZXFYtGwYcPsXQrs5Pjx43r88cfl7e2tkiVLKjQ0VImJifYuC3Zy5coVjR07VsHBwfLw8FDVqlU1ceJEZWdn27u0IkXgvcOtWrVKw4YN05gxY7R37141b95cHTt2tLmcG+4e27dv16BBg/TNN98oLi5OV65cUVRUlM6dO2fv0uAAdu/erYULF6p+/fr2LgV28ueff6pZs2YqUaKEvvjiC+3fv19Tp069Y+8uils3adIkvf3225ozZ44OHDigyZMn66233tLs2bPtXVqR4rJkd7gmTZqoUaNGmj9/vrUtJCRE3bp1U2xsrB0rgyM4efKkfH19tX37drVo0cLe5cCOzp49q0aNGmnevHn617/+pdDQUM2YMcPeZeE2e/nll/Xvf/+bTwJh9cADD8jPz0/vvfeeta179+4qWbKkli1bZsfKihYzvHewzMxMJSYmKioqyqY9KipK8fHxdqoKjiQ9PV2SVL58eTtXAnsbNGiQOnfurLZt29q7FNjR+vXrFR4ern/+85/y9fVVw4YN9c4779i7LNjR/fffr61bt+qXX36RJO3bt0+7du1Sp06d7FxZ0bLrndZwa06dOqWsrCz5+fnZtPv5+Sk1NdVOVcFRGIahESNG6P7771fdunXtXQ7saOXKldqzZ492795t71JgZ7/99pvmz5+vESNGaPTo0fruu+80ZMgQubm56YknnrB3ebCDl156Senp6apVq5acnZ2VlZWl119/XY8++qi9SytSBF4TsFgsNs8Nw8jRhrvP4MGD9cMPP2jXrl32LgV2dOzYMQ0dOlRffvml3N3d7V0O7Cw7O1vh4eF64403JEkNGzbUTz/9pPnz5xN471KrVq3SBx98oOXLl6tOnTpKSkrSsGHDVKlSJfXp08fe5RUZAu8dzMfHR87Ozjlmc9PS0nLM+uLu8txzz2n9+vXasWOHAgIC7F0O7CgxMVFpaWkKCwuztmVlZWnHjh2aM2eOLl26JGdnZztWiNvJ399ftWvXtmkLCQnRmjVr7FQR7G3kyJF6+eWX1bNnT0lSvXr1dPToUcXGxpoq8LKG9w7m6uqqsLAwxcXF2bTHxcUpMjLSTlXBngzD0ODBg7V27Vp99dVXCg4OtndJsLM2bdroP//5j5KSkqyP8PBw9erVS0lJSYTdu0yzZs1yXKrwl19+UVBQkJ0qgr2dP39eTk62cdDZ2dl0lyVjhvcON2LECPXu3Vvh4eGKiIjQwoULlZycrIEDB9q7NNjBoEGDtHz5cn366acqU6aMdfbfy8tLHh4edq4O9lCmTJkca7hLlSolb29v1nbfhYYPH67IyEi98cYb6tGjh7777jstXLhQCxcutHdpsJMuXbro9ddfV+XKlVWnTh3t3btX06ZN01NPPWXv0ooUlyUzgXnz5mny5MlKSUlR3bp1NX36dC5BdZfKa+32okWLFBMTc3uLgcNq1aoVlyW7i23YsEGjRo3SwYMHFRwcrBEjRqh///72Lgt2cubMGb3yyitat26d0tLSVKlSJT366KN69dVX5erqau/yigyBFwAAAKbGGl4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AKGZHjhyRxWJRUlKSvUux+u9//6umTZvK3d1doaGh9i4HAIoVgReA6cXExMhisejNN9+0af/kk0/yvB2z2Y0bN06lSpXSzz//rK1bt9q7nBtq1aqVhg0bZu8yANzBCLwA7gru7u6aNGmS/vzzT3uXUmQyMzMLve2hQ4d0//33KygoSN7e3kVY1Z3hVs4dgDsPgRfAXaFt27aqWLGiYmNj8+wzfvz4HB/vz5gxQ1WqVLE+j4mJUbdu3fTGG2/Iz89PZcuW1YQJE3TlyhWNHDlS5cuXV0BAgN5///0c4//3v/9VZGSk3N3dVadOHW3bts3m9f3796tTp04qXbq0/Pz81Lt3b506dcr6eqtWrTR48GCNGDFCPj4+ateuXa7HkZ2drYkTJyogIEBubm4KDQ3Vpk2brK9bLBYlJiZq4sSJslgsGj9+fK7jGIahyZMnq2rVqvLw8FCDBg308ccfW/cREBCgt99+22abPXv2yGKx6LfffpMkpaena8CAAfL19ZWnp6f+8Y9/aN++fTnO+bJly1SlShV5eXmpZ8+eOnPmjPV8b9++XTNnzpTFYpHFYtGRI0f0559/qlevXqpQoYI8PDxUo0YNLVq0KNfjKMi5A2BOBF4AdwVnZ2e98cYbmj17tn7//fdbGuurr77SiRMntGPHDk2bNk3jx4/XAw88oHLlyunbb7/VwIEDNXDgQB07dsxmu5EjR+r555/X3r17FRkZqQcffFCnT5+WJKWkpKhly5YKDQ3V999/r02bNul///ufevToYTPGkiVL5OLion//+99asGBBrvXNnDlTU6dO1ZQpU/TDDz+offv2evDBB3Xw4EHrvurUqaPnn39eKSkpeuGFF3IdZ+zYsVq0aJHmz5+vn376ScOHD9fjjz+u7du3y8nJST179tSHH35os83y5csVERGhqlWryjAMde7cWampqdq4caMSExPVqFEjtWnTRn/88Yd1m0OHDumTTz7Rhg0btGHDBm3fvt26/GTmzJmKiIhQ//79lZKSopSUFAUGBuqVV17R/v379cUXX+jAgQOaP3++fHx8bvh7y8+5A2BSBgCYXJ8+fYyuXbsahmEYTZs2NZ566inDMAxj3bp1xt//DI4bN85o0KCBzbbTp083goKCbMYKCgoysrKyrG01a9Y0mjdvbn1+5coVo1SpUsaKFSsMwzCMw4cPG5KMN99809rn8uXLRkBAgDFp0iTDMAzjlVdeMaKiomz2fezYMUOS8fPPPxuGYRgtW7Y0QkNDb3q8lSpVMl5//XWbtvvuu8949tlnrc8bNGhgjBs3Ls8xzp49a7i7uxvx8fE27X379jUeffRRwzAMY8+ePYbFYjGOHDliGIZhZGVlGffcc48xd+5cwzAMY+vWrYanp6dx8eJFmzGqVatmLFiwwDCMq+e8ZMmSRkZGhvX1kSNHGk2aNLE+b9mypTF06FCbMbp06WI8+eSTNzoNNvJ77gCYEzO8AO4qkyZN0pIlS7R///5Cj1GnTh05Of3fn08/Pz/Vq1fP+tzZ2Vne3t5KS0uz2S4iIsL6s4uLi8LDw3XgwAFJUmJior7++muVLl3a+qhVq5akqzOg14SHh9+wtoyMDJ04cULNmjWzaW/WrJl1X/mxf/9+Xbx4Ue3atbOpaenSpdZ6GjZsqFq1amnFihWSpO3btystLc06K52YmKizZ8/K29vbZozDhw/bHFOVKlVUpkwZ63N/f/8c5+56zzzzjFauXKnQ0FC9+OKLio+Pv+kx3ezcATAvF3sXAAC3U4sWLdS+fXuNHj1aMTExNq85OTnJMAybtsuXL+cYo0SJEjbPLRZLrm3Z2dk3refaVSKys7PVpUsXTZo0KUcff39/68+lSpW66Zh/H/cawzAKdEWKa7V//vnnuueee2xec3Nzs/7cq1cvLV++XC+//LKWL1+u9u3bW5cWZGdny9/fP8daZUkqW7as9efCnLuOHTvq6NGj+vzzz7Vlyxa1adNGgwYN0pQpU/LcJr/nDoD5EHgB3HXefPNNhYaG6t5777Vpr1ChglJTU23CYVFeO/ebb75RixYtJElXrlxRYmKiBg8eLElq1KiR1qxZoypVqsjFpfB/mj09PVWpUiXt2rXLui9Jio+PV+PGjfM9Tu3ateXm5qbk5GS1bNkyz36PPfaYxo4dq8TERH388ceaP3++9bVGjRopNTVVLi4uNl/8KyhXV1dlZWXlaK9QoYJiYmIUExOj5s2ba+TIkTcMvADuXgReAHedevXqqVevXpo9e7ZNe6tWrXTy5ElNnjxZjzzyiDZt2qQvvvhCnp6eRbLfuXPnqkaNGgoJCdH06dP1559/6qmnnpIkDRo0SO+8844effRRjRw5Uj4+Pvr111+1cuVKvfPOO3J2ds73fkaOHKlx48apWrVqCg0N1aJFi5SUlJTjC2Y3UqZMGb3wwgsaPny4srOzdf/99ysjI0Px8fEqXbq0+vTpI0kKDg5WZGSk+vbtqytXrqhr167WMdq2bauIiAh169ZNkyZNUs2aNXXixAlt3LhR3bp1y/cSgypVqujbb7/VkSNHVLp0aZUvX17jx49XWFiY6tSpo0uXLmnDhg0KCQnJ9/EBuLuwhhfAXem1117LsXwhJCRE8+bN09y5c9WgQQN99913eV7BoDDefPNNTZo0SQ0aNNDOnTv16aefWj/+r1Spkv79738rKytL7du3V926dTV06FB5eXnZrBfOjyFDhuj555/X888/r3r16mnTpk1av369atSoUaBxXnvtNb366quKjY1VSEiI2rdvr88++0zBwcE2/Xr16qV9+/bp4YcfloeHh7XdYrFo48aNatGihZ566inde++96tmzp44cOSI/P7981/HCCy/I2dlZtWvXVoUKFZScnCxXV1eNGjVK9evXV4sWLeTs7KyVK1cW6PgA3D0sxvV/8QEAAAATYYYXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBqBF4AAACYGoEXAAAApkbgBQAAgKkReAEAAGBq/w/kz+YiT6C9fAAAAABJRU5ErkJggg==",
      "text/plain": [
       "<Figure size 800x600 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# calculate expected mean and standard deviation\n",
    "mu=n*p\n",
    "sig=n*p*(1.-p)\n",
    "# Create a Matplotlib figure and axis\n",
    "fig, ax = plt.subplots(figsize=(8, 6))\n",
    "\n",
    "# Plot the binomial distribution with vertical bars\n",
    "ax.bar(xr, fr, width=0.5, color='red', label='Binomial distribution')\n",
    "\n",
    "# Draw the location of the mean\n",
    "ax.axvline(mu, ymin=0, ymax=np.max(fr)*3, linewidth=3, color='blue', label='Mean')\n",
    "\n",
    "# Draw a horizontal line to represent the standard deviation\n",
    "ax.hlines(np.max(fr) / 2., mu - sig, mu + sig, linewidth=2, color='green', label='Standard Deviation')\n",
    "\n",
    "# Set labels and title\n",
    "ax.set_xlabel('Number of events r')\n",
    "ax.set_ylabel('Probability')\n",
    "ax.set_title('Binomial Distribution')\n",
    "\n",
    "# Show legend\n",
    "ax.legend()\n",
    "\n",
    "# Show the Matplotlib plot\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##Pascal's Triangle is intimately connected with the binomial distribution.\n",
    "It is simply the coefficients in the expansion:\n",
    "$$ ^nC_r= \\frac{n!}{r!(n-r!)}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "           1 \n",
      "          1 1 \n",
      "         1 2 1 \n",
      "        1 3 3 1 \n",
      "       1 4 6 4 1 \n",
      "      1 5 10 10 5 1 \n",
      "     1 6 15 20 15 6 1 \n",
      "    1 7 21 35 35 21 7 1 \n",
      "   1 8 28 56 70 56 28 8 1 \n",
      "  1 9 36 84 126 126 84 36 9 1 \n",
      "22 33\n"
     ]
    }
   ],
   "source": [
    "# Print Pascal's Triangle \n",
    " \n",
    "# set number of row\n",
    "nrow = 10\n",
    "neven=0 \n",
    "nodd=0\n",
    "for i in range(nrow):\n",
    "    for j in range(nrow-i+1):\n",
    " \n",
    "        # for left spacing\n",
    "        print(end=\" \")\n",
    " \n",
    "    for j in range(i+1):\n",
    " \n",
    "        # nCr = n!/((n-r)!*r!)\n",
    "        pterm=factorial(i)//(factorial(j)*factorial(i-j))\n",
    "        print(pterm, end=\" \")\n",
    "        if (pterm % 2) == 0: neven=neven+1\n",
    "        else: nodd=nodd+1 \n",
    "    # for new line\n",
    "    print()\n",
    "print(neven,nodd)    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
