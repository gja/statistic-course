{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gaussian sample tests\n",
    "\n",
    "For convenience, here is a simple notebook that tests whether a sample of data values $x=[x_1,x_2,...]$ is drawn from an assumed Gaussian distribution. There are potentially many ways of doing this, depending on the *test statistic* we define, and what we assume we know about the Gaussian. The cells below use the three tests we discussed in Chapters 7 and 8 - sample mean z-test, sample mean t-test, and chisq test. \n",
    "\n",
    "Note that we will assume a **two tailed test** for $z$ and $t$. Its not too hard to divide/multiply by two as necessary!\n",
    "\n",
    "First the usual set up:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import *  # basic maths routines\n",
    "import numpy as np\n",
    "from scipy import stats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First the input values. Note that we define the sample manually as a list of values. It could be input other ways, for example reading a list of values from a file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "############ set values to use\n",
    "mu=2.2      # mean of assumed gaussian\n",
    "sig=1.6     # std.dev. of assumed gausssian\n",
    "alpha=0.05  # chosen significance level to test against\n",
    "x=[1.3,2.9,4.2,5.6,4.9,2.2,3.4] # define sample as a list of values\n",
    "\n",
    "N= len(x) # get the number of points in the sample.\n",
    "################"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sample mean z-test\n",
    "\n",
    "The sample mean should be equal to the hypothesised mean, but with dispersion equal to $\\sigma_\\mu=\\sigma/\\sqrt{N}$. We can then do a standard z-test."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "====Sample mean Gaussian z-test====\n",
      "hypothesis: mean= 2.2  sigma= 1.6\n",
      "test level set: 0.05  using two tailed test\n",
      "sample mean= 3.5  error= 0.605\n",
      "==> z= 2.15 P2= 0.032\n",
      "significant result\n"
     ]
    }
   ],
   "source": [
    "sampmu=np.mean(x)   # calculate sample mean\n",
    "sigmu=sig/sqrt(N)   # expected dispersion of sample mean\n",
    "z=(sampmu-mu)/sigmu \n",
    "# two tailed probability of exceeding that z-value\n",
    "P2=2.*(1.-stats.norm.cdf((abs(z)),0.,1.)) \n",
    "\n",
    "print('====Sample mean Gaussian z-test====')\n",
    "print('hypothesis: mean=',mu,' sigma=',sig)\n",
    "print('test level set:', alpha, ' using two tailed test')\n",
    "print('sample mean=',round(sampmu,3),' error=',round(sigmu,3))\n",
    "print('==> z=', round(z,3), 'P2=',round(P2,3))\n",
    "if P2<alpha:\n",
    "    print('significant result')\n",
    "else:\n",
    "    print('not significant result')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sample mean $t$-test\n",
    "\n",
    "We use the $t$-test if we only know the proposed mean, but not the dispersion, and so have to estimate the dispersion from the sample itself. Note that to apply this test we use the $1/N$ version of the sample dispersion, not the $1/(N-1)$ version. (To get the other version,, you can set ddof=1 in the numpy routine.) Note also that we are carrying through the sample mean from above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "====Sample mean Gaussian t-test====\n",
      "hypothesis: mean= 2.2\n",
      "test level set: 0.05  using two tailed test\n",
      "sample mean= 3.5  std.dev= 1.4020393310154624\n",
      "sample size N= 7  degrees of freedom nu= 6\n",
      "==> t= 2.453 P2= 0.05\n",
      "significant result at 95.0 \\% confidence\n"
     ]
    }
   ],
   "source": [
    "sampsd=np.std(x)  # normal st.dev\n",
    "t=(sampmu-mu)/(sampsd/sqrt(N)) \n",
    "dof=N-1\n",
    "# two-tailed of exceeding that t-value\n",
    "P2=2.*(1.-stats.t.cdf(t,dof))\n",
    "print('====Sample mean Gaussian t-test====')\n",
    "print('hypothesis: mean=',mu)\n",
    "print('test level set:', alpha, ' using two tailed test')\n",
    "print('sample mean=',round(sampmu,3),' std.dev=',sampsd)\n",
    "print('sample size N=',N,' degrees of freedom nu=',dof)\n",
    "print('==> t=', round(t,3), 'P2=',round(P2,3))\n",
    "if P2<alpha:\n",
    "        print('significant result at',100*(1-alpha), '\\% confidence')\n",
    "else:\n",
    "        print('Not a significant result at',100*(1-alpha), '\\% confidence')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sample scatter $\\chi^2$ test\n",
    "\n",
    "If we assume our points are all drawn from a Gaussian with known mean and variance, the observed scatter of the points around the assumed mean should be on average equal to the expected scatter. To test the significance of a large value, we use the simplest version $\\chi^2$ statistic \n",
    "\n",
    "$$\\chi^2=\\frac{1}{\\sigma_t^2} \\sum_{i=1}^N\\left(x_i-x_t \\right)^2$$\n",
    "\n",
    "where the \"true\" values $x_t=\\mu$ and $\\sigma_t=\\sigma$ from the hypothesised Gaussian, $N$ is the number of data points. Then we use the scipy routine for the chisq distribution to find the probability of getting that value or larger. Because we are using a fixed, chosen, value of $x_t$, the number of degrees of freedom is just equal to the number of data points. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "====Sample Chisq test====\n",
      "hypothesis: mean= 2.2  sigma= 1.6\n",
      "test level set: 0.05\n",
      "observed chisq value= 9.996 sample size=dof=N= 7\n",
      "Prob(>chisq)= 0.189\n",
      "not a significant result at 95.0 \\% confidence\n"
     ]
    }
   ],
   "source": [
    "sum=0.0 #initialise sum\n",
    "for i in range(0,N):\n",
    "    sum=sum+(x[i]-mu)**2\n",
    "obschisq=sum/sig**2\n",
    "\n",
    "dofchi=N\n",
    "Pchi2=1.-stats.chi2.cdf(obschisq,dofchi)  # integrated prob. above osbschisq\n",
    "\n",
    "print('====Sample Chisq test====')\n",
    "print('hypothesis: mean=',mu, ' sigma=',sig)\n",
    "print('test level set:', alpha)\n",
    "print('observed chisq value=',round(obschisq,3), \n",
    "      'sample size=dof=N=',dofchi)\n",
    "print('Prob(>chisq)=',round(Pchi2,3))\n",
    "if Pchi2<alpha:\n",
    "    print('significant result at',100*(1-alpha), '\\% confidence')\n",
    "else:\n",
    "    print('not a significant result at', 100*(1-alpha), '\\% confidence')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
