# fraction of no vote vs average disposable income, kpound
# columns:
# region-name       vote       income

Aberdeen  59.56  17.771
Angus-and-Dundee 48.675  14.925
Argyll-and-Bute  56.32   14.709
Clackmannanshire-and-Fife 54.904  14.21
Dumfries-and-Galloway     65.67  14.715
Eilean-Siar               53.42  14.069
East-and-North-Ayrshire  51.837  13.815 
Dunbartonshire           54.388 16.121
East-Lothian-and-Midlothian 59.224   16.631
Renfrewshire-and-Inverclyde  54.990 16.215
Edinburgh                    61.1   17.253
Falkirk                      53.47  14.766
Glasgow                      46.51 13.496
Inverness                    57.56 14.939
North-Lanarkshire            48.93 14.002
Orkney                       67.2  16.425
Perth-and-Kinross-and-Stirling 60.035 16.226
Borders                        66.56 15.533
Shetland                       63.71 16.609
South-Ayrshire                 57.87 15.534
South-Lanarkshire              54.67 14.954
West-Lothian                   55.18 14.678