{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction to Jupyter notebooks  \n",
    "*Version 2024 GJA*\n",
    "\n",
    "This is a Jupyter notebook - an interactive file that runs inside your web browser, and contains both text and computer code. The Jupyter system can use various different kinds of code, but in this example we are using **Python 3**\n",
    "\n",
    "The notebook is made up of a sequence of **cells**. Each cell contains either markdown (text) or code. You can \"run\" either the whole file, or a single cell. When you \"run\" a markdown cell, it just displays the text with some formatting and latex maths. When you \"run\" a code cell, it does whatever the code says. This could include some output - e.g. printing the result of a calculation, or making a plot. The output is normally displayed within the notebook itself, although there are ways to make the output appear in a separate window.\n",
    "\n",
    "This is a markdown cell. Below is our first code cell, run it, then try editing. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this is a comment line\n",
    "a=5\n",
    "b=4\n",
    "c=a*b\n",
    "print('you gave me: a=', a, 'b=',b)\n",
    "print('so a*b=',c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Editing cells\n",
    "\n",
    "You click on a cell to select it, and double-click it to edit it. The text in text cells is actually written in a very simple mark-up language called \"Markdown\". For example you make headers with ### and make some text bold with **bold**, and also put in some LateX to get equations, such as $y= a \\sin(x)$. (Double-left-click on this cell to get the idea.) Follow the **Help** menu to learn more about Markdown. When you edit a code cell, you are just writing in whatever programming language the notebook is set up for - Python by default. Again, follow the **Help** menu to brush up on your Python.\n",
    "\n",
    "Try editing this text cell, maybe changing my name (Graeme Ackland) to yours. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding cells\n",
    "\n",
    "Add a new cell with the **+** button. Note that by default a new cell is a code cell, but you can change this in the menu bar. You can also delete cells and move them around. Explore the menu."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running cells\n",
    "\n",
    "Choosing \"Cell/Run All\" executes all the cells in sequence. But you can also select a single cell, or multiple cells, and run only those, with the \"Run\" button, or by choosing \"Cell/Run Cells\". This is useful for exploring the code, because you can quickly see the effect of changing a piece of code, e.g. running something with a different parameter value. It is also useful when you are writing your own notebook, because you can test out a piece at a time.\n",
    "\n",
    "Try that now - go back up to cell no. 2, with the simple code in, change one of the input values, and run just that cell.\n",
    "\n",
    "**Caution** If you run the cells out of sequence, the value of a particular variable might not be what you thought it was!! \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reset the value of b\n",
    "b=78\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "jupyter systems have ways of inspecting the current state of the variables, like %whos and locals() But you can always add code cell and check.  commands beginning with % are called magic commands. These are special commands that provide extra functionality beyond what is available in standard Python syntax."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "what is the current value of b?\n",
      "seems to be b= 78\n",
      "Variable   Type    Data/Info\n",
      "----------------------------\n",
      "b          int     78\n"
     ]
    }
   ],
   "source": [
    "# check the current value of b\n",
    "print ('what is the current value of b?')\n",
    "print ('seems to be b=',b)\n",
    "#check everything\n",
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the result you get if you just run Cell No. 9 depends on whether you recently ran Cell No. 2 or Cell No. 6. Try making some changes and see. Of course you can avoid this problem if you always run the whole notebook. But be careful!\n",
    "\n",
    "If your outputs get in a tangle, you can go back to the start by clicking \"Kernel\" \"Restart and clear output\". This resets the outputs, it *does not* restore any edits to the code or markdown cells. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting up packages\n",
    "\n",
    "Our code examples so far use the very simplest plain Python. But just like with running Python on the command line, to get much of what you want, you need to \"import\" packages. The three most important things for us are **math** for basic maths, **numpy** for more maths stuff, and **bokeh** for plotting. There is a separate notebook for more explanation of [Setting up packages](2-setting-up-packages.ipynb), and another notebook that demonstrates [basic plotting](3-basic-plotting.ipynb) (Another common choice for plotting is **matplotlib**)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a typical set up\n",
    "from math import *\n",
    "import numpy as np\n",
    "from scipy import stats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note we have used three different ways of importing. The first way imports everything so we can just use the routine names directly; the second means that when we use numpy routines we call them \"np.blah\"; the third means we only import some of scipy, not all of it. Below is a simple example use of maths."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# example use of maths routines\n",
    "x=37\n",
    "y=log10(x)\n",
    "print('y=',y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### IPython magic\n",
    "\n",
    "Juypter notebooks use a kind of enhanced Python called *IPython*. This is short for \"Interactive Python\". This was originally intended for doing Python-y things inside a terminal window - you can issue individual Python commands, run complete Python programmes that you have written in an external file, issue standard operating system commands, and run various other useful commands, like switches that change how plots work and so on. All these things also work (in principle...) from a notebook cell.\n",
    "\n",
    "We will come across special commands when we need them. But here is something useful straight away - if you want to know more about a particular variable, rather than just printing its value, you can ask for more information, using \"name?\". Try playing with the code in the cells below.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a=7\n",
    "a       # this just echoes the value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('the value of a is ',a)  # this can give a prettier output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# but this tells us lots of stuff!\n",
    "a?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# can also try these - uncomment to make them work\n",
    "#help(a)\n",
    "#info(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
